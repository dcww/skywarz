package com.stimicode.SkyWarz.Managers;

import com.stimicode.SkyWarz.Configurations.LootConfig;
import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Main.SkyWarz;
import com.stimicode.SkyWarz.Object.LootItem;
import com.stimicode.SkyWarz.Object.SpawnLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Derrick on 6/17/2015.
 */
public class SettingsManager {
    public JavaPlugin plugin;

    public FileConfiguration config; /* Main Configuration */

    public HashMap<String, FileConfiguration> mapSpawnLocations = new HashMap<>(); /* Spawn Location Configurations */

    public FileConfiguration lootItems; /* Loot Configuration */
    public Map<String, LootItem> lootItemMap = new HashMap<>(); /* Loot Item Map - Contains all of the Loot Items. */

    public void init (JavaPlugin javaplugin) {
        plugin = javaplugin;
        config = javaplugin.getConfig();

        if (!javaplugin.getDataFolder().exists()) {
            javaplugin.getDataFolder().mkdir();
        }

        try {
            loadConfigurationFiles();
            loadConfigurationObjects();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadConfigurationFiles() throws IOException, InvalidConfigurationException {
        lootItems = loadConfigurationFile("loot.yml");
    }

    public void loadConfigurationObjects() throws IOException, InvalidConfigurationException {
        LootConfig.loadConfig(lootItems, lootItemMap);
    }

    public FileConfiguration loadConfigurationFile(String filepath) throws IOException, InvalidConfigurationException {
        File file = new File(plugin.getDataFolder().getPath()+"/"+filepath);
        if (file.exists()) {
            Log.info("Loading Configuration file:" + filepath);

            YamlConfiguration cfg = new YamlConfiguration();
            cfg.load(file);
            return cfg;
        } else {
            Log.warning("Configuration file:" + filepath + " was missing. Attempting to create.");
            file.createNewFile();
            loadConfigurationFile(filepath);
            loadDefaults(filepath);
        }
        return null;
    }

    @SuppressWarnings({"UnusedParameters", "EmptyMethod"})
    public void loadDefaults(String filepath) {

    }

    public String getStringBase(String path) throws Exception {
        return getString(plugin.getConfig(), path);
    }

    public Integer getInteger(String path) throws Exception {
        return getInteger(plugin.getConfig(), path);
    }

    public double getDouble(String path) throws Exception {
        return getDouble(plugin.getConfig(), path);
    }

    public boolean getBoolean(String path) throws Exception {
        return getBoolean(plugin.getConfig(), path);
    }

    public Integer getInteger(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration integer "+path);
        }

        return cfg.getInt(path);
    }

    public String getString(FileConfiguration cfg, String path) throws Exception {
        String data = cfg.getString(path);
        if (data == null) {
            throw new Exception("Could not get configuration string "+path);
        }
        return data;
    }

    public double getDouble(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration double "+path);
        }
        return cfg.getDouble(path);
    }

    public boolean getBoolean(FileConfiguration cfg, String path) throws Exception {
        if (!cfg.contains(path)) {
            throw new Exception("Could not get configuration boolean "+path);
        }
        return cfg.getBoolean(path);
    }

    /*public void setLocation(String config, String id, SpawnLocation loc) throws Exception {
        Location l = loc.getLocation();
        mapSpawnLocations.get(config).set("Spawns." + l.getWorld().getName() + "," + l.getBlockX() + "," + l.getBlockY() + "," + l.getBlockZ() + "," + l.getYaw() + "," + l.getPitch(), id);
        mapSpawnLocations.get(config).save(new File(SkyWarz.getPlugin().getDataFolder().getPath() + "/" + config + ".yml"));
    }*/

    /*public Location getLocation(String config, String id) {
        FileConfiguration con = mapSpawnLocations.get(config);
        ConfigurationSection configSection = con.getConfigurationSection("Spawns");
        World world = null;
        int x = 0, y = 0, z = 0;
        float yaw = 0, pitch = 0;
        for (String location : configSection.getKeys(false)) {
            String[] cords = location.split(",");
            world = Bukkit.getWorld(cords[0]);
            x = Integer.valueOf(cords[1]);
            y = Integer.valueOf(cords[2]);
            z = Integer.valueOf(cords[3]);
            yaw = Float.valueOf(cords[4]);
            pitch = Float.valueOf(cords[5]);
        }
        return new Location(world, x, y, z, yaw, pitch);
    }*/

    public void setLocation(String config, String id, SpawnLocation loc) throws Exception {
        FileConfiguration con = Manager.worldManager.worlds.get(config).getConfig();
        con.set("Spawns." + id + ".world", loc.getLocation().getWorld().getName());
        con.set("Spawns." + id + ".x", loc.getLocation().getX());
        con.set("Spawns." + id + ".y", loc.getLocation().getY());
        con.set("Spawns." + id + ".z", loc.getLocation().getZ());
        con.set("Spawns." + id + ".yaw", loc.getLocation().getYaw());
        con.set("Spawns." + id + ".pitch", loc.getLocation().getPitch());
        con.save(new File(SkyWarz.getPlugin().getDataFolder().getPath() + "/" + config + ".yml"));
    }

    public static Location getLocation(String config, String locationType, String id){
        World w = Bukkit.getServer().getWorld(Manager.settingsManager.mapSpawnLocations.get(config).getString(locationType + id + ".world"));
        double x = Manager.settingsManager.mapSpawnLocations.get(config).getDouble(locationType + id + ".x");
        double y = Manager.settingsManager.mapSpawnLocations.get(config).getDouble(locationType + id + ".y");
        double z = Manager.settingsManager.mapSpawnLocations.get(config).getDouble(locationType + id + ".z");
        float yaw = (float) Manager.settingsManager.mapSpawnLocations.get(config).getDouble(locationType + id + ".yaw");
        float pitch = (float) Manager.settingsManager.mapSpawnLocations.get(config).getDouble(locationType + id + ".pitch");
        return new Location(w, x, y, z, yaw, pitch);
    }
}
