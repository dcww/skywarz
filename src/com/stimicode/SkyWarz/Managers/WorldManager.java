package com.stimicode.SkyWarz.Managers;

import com.stimicode.SkyWarz.Game.Game;
import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Main.SkyWarz;
import com.stimicode.SkyWarz.Object.MultiWorld;
import com.stimicode.SkyWarz.Object.SpawnLocation;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.*;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import sun.management.snmp.jvmmib.JvmMemManagerTableMeta;

import java.io.*;
import java.util.*;

/**
 * Created by Derrick on 8/5/2015.
 */
public class WorldManager {

    public HashMap<String, MultiWorld> worlds = new HashMap<>();

    public void setupMaps() {
        ConfigurationSection configSection = Manager.settingsManager.config.getConfigurationSection("Worlds");
        int x = 0;

        for (String mapName : configSection.getKeys(false)) {
            Manager.worldManager.copyWorld(new File("backup/" + mapName), new File(mapName));
            MultiWorld multiWorld = new MultiWorld(mapName, createWorld(mapName));
            multiWorld.loadConfig();

            if (configSection.getString(mapName + ".type").equalsIgnoreCase("game")) {
                multiWorld.loadSpawns();
                multiWorld.setIsGameWorld(true);
                Log.debug("World [" + multiWorld.getName() + "] returned Game as type.");
                x++;
            }
            worlds.put(mapName, multiWorld);
        }
        Game.mapNumber = x;
    }

    public void saveWorldToTemplate(String worldName) {
        unloadWorld(worldName);
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SkyWarz.getPlugin(), () -> {
            copyWorld(new File(worldName), new File("backup/" + worldName));
            MultiWorld multiWorld = new MultiWorld(worldName, createWorld(worldName));
            multiWorld.loadConfig();

            if (Manager.settingsManager.config.get("worlds." + worldName + ".type") == "game") {
                multiWorld.loadSpawns();
                multiWorld.setIsGameWorld(true);
            }
            worlds.put(worldName, multiWorld);
        }, 100L);
    }

    public void removeWorld(String worldName) {
        unloadWorld(worldName);
        TaskManager.syncTask(() -> {
            Log.info("[World Manager]: Preparing to delete world " + worldName + ".");
            File file = new File(worldName);
            try {
                deleteFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Log.info("[World Manager]: " + worldName + " has been deleted!");
        }, 100L);
        Manager.settingsManager.config.set("Worlds." + worldName, null);
        try {
            Manager.settingsManager.config.save(new File(SkyWarz.getPlugin().getDataFolder().getPath() + "/config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteFile(File f) throws FileNotFoundException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                deleteFile(c);
            }
        }
        if (!(f.delete())) {
            throw new FileNotFoundException("Failed to delete file: " + f);
        }
    }

    public void resetWorld(String worldName) {
        unloadWorld(worldName);
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(SkyWarz.getPlugin(), () -> {
            copyWorld(new File("backup/" + worldName), new File(worldName));
            MultiWorld multiWorld = new MultiWorld(worldName, createWorld(worldName));
            multiWorld.loadConfig();

            if (Manager.settingsManager.config.get("worlds." + worldName + ".type") == "game") {
                multiWorld.loadSpawns();
                multiWorld.setIsGameWorld(true);
            }
            worlds.put(worldName, multiWorld);
        }, 100L);
    }

    public void createNewWorld(String worldName, String template) {
        copyWorld(new File("backup/" + template), new File(worldName));
        MultiWorld multiWorld = new MultiWorld(worldName, createWorld(worldName));
        multiWorld.loadConfig();

        Manager.settingsManager.config.set("Worlds." + worldName + ".type", "default");
        try {
            Manager.settingsManager.config.save(new File(SkyWarz.getPlugin().getDataFolder().getPath() + "/config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        worlds.put(worldName, multiWorld);
    }

    private World createWorld(String name) {
        World world;
        world = Bukkit.getServer().getWorld(name);
        if (world == null) {
            Log.info("[World Manager]: Building world object for " + name + ".");
            WorldCreator wc = new WorldCreator(name);
            wc.environment(World.Environment.NORMAL);
            wc.type(WorldType.FLAT);
            wc.generateStructures(false);

            world = Bukkit.getServer().createWorld(wc);
            world.setAutoSave(false);
            world.setSpawnFlags(false, false);
            world.setKeepSpawnInMemory(false);
            Log.info("[World Manager]: Finished building world Object.");
        }
        return world;
    }

    private void copyWorld(File source, File target) {
        try {
            ArrayList<String> ignore = new ArrayList<>(Arrays.asList("uid.dat", "session.dat"));
            if (!(ignore.contains(source.getName()))) {
                if (source.isDirectory()) {                             //If the Source File is a directory and not a file.
                    if (!target.exists()) {                             //If Target Directory doesn't exist, let's create it.
                        target.mkdirs();
                        Log.info("[File Manager]: Directory copied from " + source + " to " + target + "!");
                    }

                    String files[] = source.list();                     //Grab the list of this directories contents.

                    for (String file : files) {
                        File srcFile = new File(source, file);          //Create File for the Source File.
                        File destFile = new File(target, file);         //Create File for the Target/Destination File.
                        copyWorld(srcFile, destFile);                   //Recursively Copy (Overwrite) the Files.
                    }
                } else {                                                //If the Source File is not a directory and just a file.
                    InputStream in = new FileInputStream(source);       //Use Bytes Stream to support all file types.
                    OutputStream out = new FileOutputStream(target);    //""

                    byte[] buffer = new byte[1024];
                    int length;
                    while ((length = in.read(buffer)) > 0) {            //Cope the Files Content in bytes.
                        out.write(buffer, 0, length);
                    }
                    in.close();
                    out.close();
                    Log.info("[File Manager]: File Copied from [[" + source + "]] to [[" + target + "]]!");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void unloadWorld(String _world) {
        World world = Bukkit.getWorld(_world);
        if (world == null) {
            return;
        }
        Log.info("[World Manager]: Unloading world " + _world + ".");
        worlds.remove(_world);
        for (Player player : world.getPlayers()) {
            player.teleport(Bukkit.getWorld("world").getSpawnLocation());
            MessageManager.send(player, C.B_Gold + "[World Manager]: " + C.Aqua+"You have been moved to" + C.Green + " World " + C.Aqua + "while " + C.Green + _world + C.Aqua + " is unloaded.");
        }
        world.save();
        Bukkit.getServer().unloadWorld(world, false);
        Log.info("[World Manager]: Finished unloading world " + _world + ".");
    }
}
