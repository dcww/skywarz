package com.stimicode.SkyWarz.Managers;

import com.gmail.filoghost.holograms.api.Hologram;
import com.gmail.filoghost.holograms.api.HolographicDisplaysAPI;
import com.stimicode.SkyWarz.APIs.TitleAPI.Title;
import com.stimicode.SkyWarz.Account.CoreAccount;
import com.stimicode.SkyWarz.Main.Global;
import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Main.SkyWarz;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Derrick on 6/18/2015.
 */
public class MessageManager {
    /* Stores the player name and the hash code of the last message sent to prevent error spamming the player. */
    private static HashMap<String, Integer> lastMessageHashCode = new HashMap<>();

    public static void sendErrorNoRepeat(Object sender, String line) {
        if (sender instanceof Player) {
            Player player = (Player)sender;
            Integer hashcode = lastMessageHashCode.get(player.getName());
            if (hashcode != null && hashcode == line.hashCode()) {
                return;
            }
            lastMessageHashCode.put(player.getName(), line.hashCode());
        }
        send(sender, C.Red+line);
    }

    public static void sendError(Object sender, String line) {
        send(sender, C.Red + line);
    }

    /*
     * Sends message to playerName(if online) AND console.
     */
    @SuppressWarnings("deprecation")
    public static void console(String playerName, String line) {
        try {
            Player player = Global.getPlayer(playerName);
            send(player, line);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.info(line);
    }

    public static void send(Object sender, String line) {
        if ((sender instanceof Player)) {
            ((Player) sender).sendMessage(line);
        } else if (sender instanceof CommandSender) {
            ((CommandSender) sender).sendMessage(line);
        }
        else if (sender instanceof CoreAccount) {
            try {
                Global.getPlayer(((CoreAccount) sender)).sendMessage(line);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void send(Object sender, String[] lines) {
        boolean isPlayer = false;
        if (sender instanceof Player)
            isPlayer = true;

        for (String line : lines) {
            if (isPlayer) {
                ((Player) sender).sendMessage(line);
            } else {
                ((CommandSender) sender).sendMessage(line);
            }
        }
    }

    public static String buildTitle(String title) {
        String line =   "-------------------------------------------------------";
        String titleBracket = "[ " + C.Green + C.BOLD + title + C.Aqua + C.BOLD + " ]";

        if (titleBracket.length() > line.length()) {
            return C.Aqua + C.BOLD + "-" + titleBracket + "-";
        }
        int min = (line.length() / 2) - titleBracket.length() / 2;
        int max = (line.length() / 2) + titleBracket.length() / 2;
        String out = C.Aqua + C.BOLD + line.substring(0, Math.max(0, min));
        out += titleBracket + line.substring(max);
        return out;
    }

    public static String buildSmallTitle(String title) {
        String line =   C.Aqua + C.BOLD + "------------------------------";
        String titleBracket = "[ "+title+" ]";
        int min = (line.length() / 2) - titleBracket.length() / 2;
        int max = (line.length() / 2) + titleBracket.length() / 2;
        String out = C.Aqua + C.BOLD + line.substring(0, Math.max(0, min));
        out += titleBracket + line.substring(max);
        return out;
    }

    public static void sendSubHeading(CommandSender sender, String title) {
        send(sender, buildSmallTitle(title));
    }

    public static void sendHeading(CommandSender sender, String title) {
        send(sender, buildTitle(title));
    }

    public static void sendSuccess(CommandSender sender, String message) {
        send(sender, C.Green + message);
    }

    public static void send(CommandSender sender, List<String> outs) {
        for (String str : outs) {
            send(sender, str);
        }
    }

    public static void sendChat(CoreAccount account, String format, String message) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            String msg = String.format(format, account.getName(), message);
            player.sendMessage(msg);
        }
    }

    public static void sendAll(String str) {
        for (Player player : Bukkit.getOnlinePlayers()) {
            player.sendMessage(str);
        }
    }

    public static void sendSuccess(CoreAccount account, String message) {
        try {
            Player player = Global.getPlayer(account);
            sendSuccess(player, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void administrator(CoreAccount account, String... message) {
        if (account.hasRank(Rank.Administrator, false)) {
            send(account, C.B_Red+"[Admin] "+message);
        }
    }

    public static void sendTitle(CoreAccount account, String message, String sub) {
        Player player = null;
        try {
            player = Global.getPlayer(account);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (player == null) {
            return;
        }

        Title.sendTitle(player, 20, 100, 20, message, sub);
    }

    public static void sendHoloMessage(Location loc, int seconds, String... line) {
        Hologram holo = HolographicDisplaysAPI.createHologram(SkyWarz.getPlugin(), loc, line);

        class SyncDelay implements Runnable {
            Hologram holo;

            public SyncDelay(Hologram holo) {
                this.holo = holo;
            }

            @Override
            public void run() {
                holo.delete();
            }
        }

        TaskManager.syncTask(new SyncDelay(holo), 20*seconds);
    }
}
