package com.stimicode.SkyWarz.Managers.Gui;

import com.stimicode.SkyWarz.Command.CommandBase;
import com.stimicode.SkyWarz.Main.Log;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * Created by Derrick on 9/12/2015.
 */
public class CommandButton implements GuiAction {

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        String className = GuiItem.getActionData(stack, "className");
        String commandString = GuiItem.getActionData(stack, "command");
        String[] args = commandString.split(" ");

        try {
            Class<?> myClass = Class.forName(className);
            Constructor<?> constructor = myClass.getConstructor();
            CommandBase instance = (CommandBase) constructor.newInstance();
            instance.onCommand((Player) event.getWhoClicked(), null, "button", args);

        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException |
                InstantiationException | IllegalAccessException | IllegalArgumentException |
                InvocationTargetException e) {
            Log.error("Message:" + e.getMessage());
            e.printStackTrace();
        }

        Player player = (Player)event.getWhoClicked();
        player.closeInventory();
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
    }
}
