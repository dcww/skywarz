package com.stimicode.SkyWarz.Managers.Gui;


import com.stimicode.SkyWarz.Util.AttributeUtil;
import com.stimicode.SkyWarz.Util.ItemManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Derrick on 6/17/2015.
 */
public class GuiItem {
    public static final int MAX_INV_SIZE = 54;
    public static final int INV_ROW_COUNT = 9;
    public static final int SINGLE_CHEST_INV_SIZE = 9*3;

    private static HashMap<String, Inventory> savedInventories = new HashMap<String, Inventory>();

    public static Inventory getGUIInventory(String title) {
        return savedInventories.get(title);
    }

    public static Inventory createGUIInventory(int size, String title, boolean save) {
        Inventory inv = createGUIInventory(size, title);
        if (save) {
            savedInventories.put(title, inv);
        }
        return inv;
    }

    public static Inventory createGUIInventory(int size, String title) {
        title = title.substring(0, Math.min(title.length(), 31));
        Inventory inv = Bukkit.createInventory(new GuiInventoryHolder(), size, title);
        return inv;
    }

    public static Inventory createGUIInventory(int size, String title, boolean allowPlacement, InventoryCallback closeCallback) {
        title = title.substring(0, Math.min(title.length(), 31));
        GuiInventoryHolder holder = new GuiInventoryHolder();
        holder.allowPlacement = allowPlacement;
        holder.callbacks = closeCallback;
        Inventory inv = Bukkit.createInventory(holder, size, title);
        return inv;
    }

    public static ItemStack getGUIItem(String title, String[] messages, int type, int data) {
        ItemStack stack = ItemManager.createItemStack(type, 1, (short) data);
        AttributeUtil attrs = new AttributeUtil(stack);
        attrs.removeAll();
        //attrs.add(Attribute.newBuilder().name("Damage").type(AttributeType.GENERIC_ATTACK_DAMAGE).amount(0).build());
		/* Hide everything. */
        attrs.setHideFlag(Integer.MAX_VALUE);

        attrs.setItemProperty("GUI", title);
        attrs.setName(title);
        attrs.setLore(messages);
        return attrs.getStack();
    }

    public static ItemStack getGUIItem(String title, String[] messages, Material materialType, int data) {
        ItemStack stack = ItemManager.createItemStack(materialType, 1, (short) data);
        AttributeUtil attrs = new AttributeUtil(stack);
        attrs.removeAll();
        //attrs.add(Attribute.newBuilder().name("Damage").type(AttributeType.GENERIC_ATTACK_DAMAGE).amount(0).build());
		/* Hide everything. */
        attrs.setHideFlag(Integer.MAX_VALUE);

        attrs.setItemProperty("GUI", title);
        attrs.setName(title);
        attrs.setLore(messages);
        return attrs.getStack();
    }

    public static boolean isGUIItem(ItemStack stack) {
        AttributeUtil attrs = new AttributeUtil(stack);
        String title = attrs.getItemProperty("GUI");
        if (title != null) {
            return true;
        }
        return false;
    }

    public static ItemStack setActionClass(ItemStack stack, Class<?> clazz) {
        AttributeUtil attrs = new AttributeUtil(stack);

        attrs.setItemProperty("GUI_ACTION", clazz.getSimpleName());
        if (!clazz.getPackage().getName().equals("com.stimicode.SkyWarz.Gui")) {
            attrs.setItemProperty("GUI_ACTION_PACKAGE", clazz.getPackage().getName());
        }
        return attrs.getStack();
    }

    @Deprecated
    public static ItemStack setAction(ItemStack stack, String action) {
        AttributeUtil attrs = new AttributeUtil(stack);
        attrs.setItemProperty("GUI_ACTION", action);
        return attrs.getStack();
    }

    public static String getAction(ItemStack stack) {
        AttributeUtil attrs = new AttributeUtil(stack);
        String action = attrs.getItemProperty("GUI_ACTION");
        return action;
    }

    @Deprecated
    public static ItemStack setActionPackage(ItemStack stack, String actionPackage) {
        AttributeUtil attrs = new AttributeUtil(stack);
        attrs.setItemProperty("GUI_ACTION_PACKAGE", actionPackage);
        return attrs.getStack();
    }

    public static String getActionPackage(ItemStack stack, String server) {
        AttributeUtil attrs = new AttributeUtil(stack);
        String action = attrs.getItemProperty("GUI_ACTION_PACKAGE");
        if (action == null) {
            //if (server.equalsIgnoreCase("hub")) {
                return "com.stimicode.SkyWarz.Gui";
            //}
        }
        return action;
    }

    public static ItemStack setActionData(ItemStack stack, String key, String value) {
        AttributeUtil attrs = new AttributeUtil(stack);
        attrs.setItemProperty("GUI_ACTION_DATA:" + key, value);
        return attrs.getStack();
    }

    public static String getActionData(ItemStack stack, String key) {
        AttributeUtil attrs = new AttributeUtil(stack);
        String data = attrs.getItemProperty("GUI_ACTION_DATA:" + key);
        return data;
    }

    public static ItemStack addLore(ItemStack stack, String lore) {
        AttributeUtil attrs = new AttributeUtil(stack);
        attrs.addLore(lore);
        return attrs.getStack();
    }

    public static ItemStack setLore(ItemStack stack, String[] lores) {
        AttributeUtil attrs = new AttributeUtil(stack);
        attrs.setLore(lores);
        return attrs.getStack();
    }

    public static ItemStack build(String title, int type, int data, String... messages) {
        return getGUIItem(title, messages, type, data);
    }

    public static ItemStack build(String title, Material type, int data, String... messages) {
        return getGUIItem(title, messages, type, data);
    }

    public static ItemStack asGuiItem(ItemStack stack) {
        AttributeUtil attrs = new AttributeUtil(stack);
        attrs.setItemProperty("GUI", "" + ItemManager.getId(stack));
        return attrs.getStack();
    }

    public static void processAction(String actionPackage, String action, ItemStack stack, InventoryClickEvent event) {

		/* Get class name from reflection and perform assigned action */
        try {
            Class<?> clazz = Class.forName(actionPackage+"."+action);
            Constructor<?> constructor = clazz.getConstructor();
            GuiAction instance = (GuiAction) constructor.newInstance();
            instance.performAction(event, stack);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void processAction(String actionPackage, String action, ItemStack stack, PlayerInteractEvent event) {

		/* Get class name from reflection and perform assigned action */
        try {
            Class<?> clazz = Class.forName(actionPackage+"."+action);
            Constructor<?> constructor = clazz.getConstructor();
            GuiAction instance = (GuiAction) constructor.newInstance();
            instance.performAction(event, stack);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static ItemStack build(String title, int id, int data, ArrayList<String> infoMessages) {
        String[] strArray = new String[infoMessages.size()];
        infoMessages.toArray(strArray);
        return getGUIItem(title, strArray, id, data);
    }

    public static ItemStack build(String title, Material typeMaterial, int data, ArrayList<String> infoMessages) {
        String[] strArray = new String[infoMessages.size()];
        infoMessages.toArray(strArray);
        return getGUIItem(title, strArray, typeMaterial, data);
    }

    public static void addItemAtSlotNoClobber(Inventory inv, int slot, ItemStack stack) {
        ItemStack savedStack = inv.getItem(slot);
        inv.setItem(slot, stack);
        if (savedStack != null) {
            inv.addItem(savedStack);
        }
    }

    public static ItemStack addLoreFormatted(ItemStack stack, String input) {
        for (String str : toLore(input)) {
            stack = GuiItem.addLore(stack, str);
        }
        //stack = LoreGuiItem.setLore(stack, toLore(input));
        return stack;
    }

    public static String[] toLore(String input) {
        String lastCharCode = "";
        int maxCharsPerColumn = 35;

        ArrayList<String> lines = new ArrayList<String>();

        String currentLine = "";
        int lineCharCount = 0;
        for (String word : input.split(" ")) {
            String wordLastColors = ChatColor.getLastColors(word);
            if (wordLastColors.length() > 0) {
                lastCharCode = wordLastColors;
            }

            if (word.length() < maxCharsPerColumn) {
                if ((lineCharCount + word.length()) > maxCharsPerColumn) {
					/*
					 * This word would overrun the lore item move to
					 * next line.
					 */
                    lines.add(currentLine);
                    currentLine = lastCharCode+word+" ";
                    lineCharCount = word.length()+1;
                } else {
					/* word can fit in this line. */
                    currentLine += word+" ";
                    lineCharCount += word.length()+1;
                }
            } else {
				/* Split apart individual words that are too long. */
                String firstPart = word.substring(0, maxCharsPerColumn);
                String secondPart = word.substring(maxCharsPerColumn, word.length());
                lines.add(lastCharCode+firstPart); // Will always be entire row...

                while (secondPart.length() >= maxCharsPerColumn) {
					/* We have to keep iterating. the second part may still be too long. */
                    String tmp = secondPart.substring(0, maxCharsPerColumn);
                    secondPart = secondPart.substring(maxCharsPerColumn, secondPart.length());

                    lines.add(lastCharCode+tmp); // Will always be entire row.
                }

                lines.add(lastCharCode+secondPart);
                lineCharCount = secondPart.length(); /* put down remaining second part. */
            }
        }
        lines.add(lastCharCode+currentLine);
        String[] strArray = new String[lines.size()];
        lines.toArray(strArray);
        return strArray;
    }
}