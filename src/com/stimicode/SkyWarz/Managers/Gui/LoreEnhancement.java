package com.stimicode.SkyWarz.Managers.Gui;

import com.stimicode.SkyWarz.Util.AttributeUtil;
import com.stimicode.SkyWarz.Util.ItemManager;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

/**
 * Created by Derrick on 6/24/2015.
 */
public abstract class LoreEnhancement {
    public AttributeUtil add(AttributeUtil attrs) {
        return attrs;
    }

    public AttributeUtil remove(AttributeUtil attrs) {
        return attrs; //broken??? WTF?
    }

    public static HashMap<String, LoreEnhancement> enhancements = new HashMap<String, LoreEnhancement>();
    public HashMap<String, String> variables = new HashMap<String, String>();

    public static void init() {
        enhancements.put("LoreEnhancementSoulBound", new LoreEnhancementSoulBound());
    }

    public boolean onDeath(PlayerDeathEvent event, ItemStack stack) { return false; }

    public boolean canEnchantItem(ItemStack item) {
        return true;
    }

    public static boolean isWeaponOrArmor(ItemStack item) {
        return ItemManager.isWeapon(item) || ItemManager.isArmor(item);
    }

    public boolean hasEnchantment(ItemStack item) {
        return false;
    }

    public String getDisplayName() {
        return "LoreEnchant";
    }

    public double getLevel(AttributeUtil attrs) {	return 0; }
    public abstract String serialize(ItemStack stack);
    public abstract ItemStack deserialize(ItemStack stack, String data);
}