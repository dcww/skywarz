package com.stimicode.SkyWarz.Managers.Gui;

import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/**
 * Created by Derrick on 6/17/2015.
 */
public class GuiInventoryHolder implements InventoryHolder {
    Inventory inv = null;
    public InventoryCallback callbacks = null;
    public boolean allowPlacement = false;

    @Override
    public Inventory getInventory() {
        return inv;
    }
}
