package com.stimicode.SkyWarz.Managers.Gui;

import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 6/17/2015.
 */
public interface GuiAction {
    void performAction(InventoryClickEvent event, ItemStack stack);
    void performAction(PlayerInteractEvent event, ItemStack stack);
}
