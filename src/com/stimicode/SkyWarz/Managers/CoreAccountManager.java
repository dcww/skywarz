package com.stimicode.SkyWarz.Managers;

import com.stimicode.SkyWarz.Account.CoreAccount;
import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.SQL.SQL;
import com.stimicode.SkyWarz.Tasks.PlayerKickBan;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Derrick on 6/21/2015.
 */
public class CoreAccountManager implements Listener {
    public JavaPlugin Plugin;
    public HashMap<String, CoreAccount> AccountProfiles = new HashMap<>();

    public void init(JavaPlugin plugin) {
        Plugin = plugin;
        Plugin.getServer().getPluginManager().registerEvents(this, Plugin);
    }

    @EventHandler
    public void AsyncLogin(AsyncPlayerPreLoginEvent event) {
        CoreAccount account = null;
        try {
            Log.info("Account UUID from AsyncLogin: " + event.getUniqueId().toString());
            account = getAccountUUID(event.getUniqueId());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.info("Beginning Async PlayerListeners Pre-Login Task for " + event.getUniqueId().toString() + ".");
    }

    @EventHandler
    public void PlayerLoginEvent(PlayerLoginEvent event) {
        class AsyncTask implements Runnable {
            UUID l_uuid;
            public AsyncTask (UUID uuid) {
                this.l_uuid = uuid;
            }
            public Player getPlayer() throws Exception {
                Player player = Bukkit.getPlayer(l_uuid);
                if (player == null) {
                    throw new Exception("Player is currently not online.");
                }

                return player;
            }

            @Override
            public void run() {
                try {
                    Log.info("Beginning Async PlayerListeners Login Task for "+event.getPlayer().getName()+". ("+event.getPlayer().getUniqueId().toString()+")");
                    CoreAccount account = getAccountUUID(event.getPlayer().getUniqueId());

                    if (getAccount(getPlayer().getName()) != account) {
                        account.handleNameChange(getPlayer().getName());
                    }

                    //This should mean that this is the first time the person has logged in... Creating profile..
                    if (account == null) {
                        Log.info("We were unable to locate the CoreAccount Profile for " + event.getPlayer().getName() + ". Beginning the CoreAccount Profile Creation process.");
                        try {
                            account = new CoreAccount(event.getPlayer().getName(), event.getPlayer().getUniqueId());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        addCoreAccount(account);
                        Log.info("Initial CoreAccount Profile Creation successful for " + event.getPlayer().getName() + ".");
                        account.setRegistered(System.currentTimeMillis());
                        account.setRank(Rank.Default);
                    }

                    if (account.getUuid() == null) {
                        account.setUuid(getPlayer().getUniqueId().toString());
                        Log.info("CoreAccount Profile named:" + account.getName() + " was acquired by UUID:" + account.getUuidString());
                    } else if (!account.getUuid().equals(getPlayer().getUniqueId())) {
                        TaskManager.syncTask(new PlayerKickBan(getPlayer().getUniqueId(), true, false, "You're attempting to log in with a name already in use. Please contact a Server Administrator immediately."));
                        return;
                    }

                    account.setLastOnline(System.currentTimeMillis());
                    account.setIP(getPlayer().getAddress().getAddress().getHostAddress());

                    try {
                        account.saveNow();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        TaskManager.asyncTask("onPlayerLogin-" + event.getPlayer().getName(), new AsyncTask(event.getPlayer().getUniqueId()), 5);
    }

    public void loadAccounts() throws SQLException {
        Connection context = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        try {
            context = SQL.getConnection();
            ps = context.prepareStatement("SELECT * FROM "+SQL.tb_prefix+CoreAccount.TABLE_NAME);
            rs = ps.executeQuery();

            while(rs.next()) {
                CoreAccount account;
                try {
                    account = new CoreAccount(rs);
                    addAccount(account);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Log.info("Loaded "+AccountProfiles.size()+" Profile Accounts!");
        } finally {
            SQL.close(rs, ps, context);
        }
    }

    public void addCoreAccount(CoreAccount account) {
        AccountProfiles.put(account.getUuidString(), account);
    }

    public CoreAccount getAccount(Player player) {
        return AccountProfiles.get(player.getUniqueId().toString());
    }

    public CoreAccount getAccount(CoreAccount account) {
        return AccountProfiles.get(account.getName().toLowerCase());
    }

    public CoreAccount getAccount(String name) {
        for (CoreAccount account : AccountProfiles.values()) {
            if (account.getName().equalsIgnoreCase(name)) {
                return account;
            }
        }
        return null;// AccountProfiles.get(name.toLowerCase());
    }

    public boolean containsAccount(String name) {
        return AccountProfiles.containsKey(name.toLowerCase());
    }

    public void addAccount(CoreAccount account) {
        AccountProfiles.put(account.getUuidString(), account);
    }

    public void removeAccount(CoreAccount account) {
        AccountProfiles.remove(account.getName().toLowerCase());
    }

    public CoreAccount getAccountUUID(UUID uid) {
        return AccountProfiles.get(uid.toString());
    }

    public Collection<CoreAccount> getAccounts() {
        return AccountProfiles.values();
    }

    public static Player getPlayer(CoreAccount account) throws Exception {
        Player player = Bukkit.getPlayer(account.getUuid());
        if (player == null) {
            throw new Exception("PlayerListeners "+account.getName()+" could not be located.");
        }
        return player;
    }

    public static Player getPlayer(UUID uuid) throws Exception {
        Player player = Bukkit.getPlayer(uuid);
        if (player == null) {
            throw new Exception("PlayerListeners could not be located.");
        }
        return player;
    }

    @Deprecated //Deprecated because Minecraft now allows name changes, getting a player based on name is no longer reliable.
    public static Player getPlayer(String name) throws Exception {
        Player player = Bukkit.getPlayer(name);
        if (player == null) {
            throw new Exception("PlayerListeners "+name+" could not be located.");
        }
        return player;
    }
}
