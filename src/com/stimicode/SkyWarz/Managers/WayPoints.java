package com.stimicode.SkyWarz.Managers;

import com.stimicode.SkyWarz.Main.SkyWarz;
import com.stimicode.SkyWarz.Object.SpawnLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.File;

/**
 * Created by Derrick on 8/3/2015.
 */
public class WayPoints {

    public static void setLocation(String config, String id, SpawnLocation loc) throws Exception {
        Manager.settingsManager.mapSpawnLocations.get(config).set("spawns." + id + ".world", loc.getLocation().getWorld().getName());
        Manager.settingsManager.mapSpawnLocations.get(config).set("spawns." + id + ".x", loc.getLocation().getX());
        Manager.settingsManager.mapSpawnLocations.get(config).set("spawns." + id + ".y", loc.getLocation().getY());
        Manager.settingsManager.mapSpawnLocations.get(config).set("spawns." + id + ".z", loc.getLocation().getZ());
        Manager.settingsManager.mapSpawnLocations.get(config).set("spawns." + id + ".yaw", loc.getLocation().getYaw());
        Manager.settingsManager.mapSpawnLocations.get(config).set("spawns." + id + ".pitch", loc.getLocation().getPitch());
        Manager.settingsManager.mapSpawnLocations.get(config).save(new File(SkyWarz.getPlugin().getDataFolder().getPath() + "/" + config + ".yml"));
    }

    public static Location getLocation(String config, String id){
        World w = Bukkit.getServer().getWorld(Manager.settingsManager.mapSpawnLocations.get(config).getString("spawns." + id + ".world"));
        double x = Manager.settingsManager.mapSpawnLocations.get(config).getDouble("spawns." + id + ".x");
        double y = Manager.settingsManager.mapSpawnLocations.get(config).getDouble("spawns." + id + ".y");
        double z = Manager.settingsManager.mapSpawnLocations.get(config).getDouble("spawns." + id + ".z");
        float yaw = (float) Manager.settingsManager.mapSpawnLocations.get(config).getDouble("spawns." + id + ".yaw");
        float pitch = (float) Manager.settingsManager.mapSpawnLocations.get(config).getDouble("spawns." + id + ".pitch");
        return new Location(w, x, y, z, yaw, pitch);
    }
}
