package com.stimicode.SkyWarz.Managers;

import com.stimicode.SkyWarz.Object.LootItem;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.inventory.ItemStack;

import java.util.HashSet;
import java.util.Random;

/**
 * Created by Derrick on 7/28/2015.
 */
public class ChestManager {

    public HashSet<Location> filledChests = new HashSet<>();

    @SuppressWarnings("deprecation")
    public void fillChest(Chest chest) {
        chest.getInventory().clear();
        int temp = 0;
        for (LootItem item : Manager.settingsManager.lootItemMap.values()) {
            if (temp == 8) {
                return;
            }

            Random rand = new Random();
            int chance = rand.nextInt(99)+1;
            if (chance <= item.getChance()) {
                ItemStack itemStack = new ItemStack(item.getMaterial(), item.getAmount(), (short)0, item.getData());
                chest.getInventory().addItem(itemStack);
                temp++;
            }
        }
    }


}
