package com.stimicode.SkyWarz.Managers;

import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Derrick on 7/27/2015.
 */
public class Manager {

    public static JavaPlugin plugin;

    public static CoreAccountManager accountManager;
    public static GameStateManager gameStateManager;
    public static SettingsManager settingsManager;
    public static ChestManager chestManager;
    public static WorldManager worldManager;

    public static void init(JavaPlugin plug) {
        plugin = plug;
        accountManager = new CoreAccountManager();
        accountManager.init(plugin);

        gameStateManager.setState(GameStateManager.Lobby);

        settingsManager = new SettingsManager();

        chestManager = new ChestManager();

        worldManager = new WorldManager();
    }
}
