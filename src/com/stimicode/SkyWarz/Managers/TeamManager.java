package com.stimicode.SkyWarz.Managers;

import com.stimicode.SkyWarz.Object.Team;
import org.bukkit.Color;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by Derrick on 8/6/2015.
 */
public class TeamManager implements Listener {

    public HashSet<Team> teams = new HashSet<>();

    /**
     * Create and Add a new Team.
     * @param name Requires a Team Name. (String)
     * @param color Requires a Team Color. (Color)
     */
    public void addTeam(String name, Color color) {
        teams.add(new Team(name, color));
    }

    /**
     * Remove a Team.
     * @param name Requires a Team Name. (String)
     */
    public void removeTeam(String name) {
        teams.remove(name);
    }

    /**
     * Grabs a collection of all of the teams.
     * @return Returns a collection of the teams.
     */
    public Collection<Team> getTeams() {
        return this.teams;
    }

    /**
     * Checks if the player has a team.
     * @param player Requires a given player object.
     * @return Returns whether or not this player has a team.
     */
    public boolean playerHasTeam(Player player) {
        for (Team team : this.teams) {
            if (team.hasPlayer(player)) {
                return true;
            }
        }
        return false;
    }

    //TODO If this given player is not on a team, this will return null, make sure to check for that!
    /**
     * Gets a players team.
     * @param player Requires a given player object.
     * @return Returns the given players team or null if the player is not on a team.
     */
    public Team getPlayerTeam(Player player) {
        for (Team team : this.teams) {
            if (team.hasPlayer(player)) {
                return team;
            }
        }
        return null;
    }

    /**
     * Checks if two players are on the same team.
     * @param player1 Requires a given player object.
     * @param player2 Requires a given player object.
     * @return Returns whether or not both of the given player objects are on the same team.
     * @throws Exception If one or both of the given players do not have a team this will throw an exception.
     */
    public boolean sameTeam(Player player1, Player player2) throws Exception {
        if (playerHasTeam(player1) && playerHasTeam(player2)) {
            return getPlayerTeam(player1) == getPlayerTeam(player2);
        } else {
            throw new Exception("One or both of the given players do not have a team.");
        }
    }

    @EventHandler
    public void entityDamageEvent(EntityDamageByEntityEvent event) throws Exception {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }

        if (!(event.getDamager() instanceof Player)) {
            return;
        }

        Player player = (Player) event.getEntity();
        Player damager = (Player) event.getDamager();

        if (sameTeam(player, damager)) {
            event.setCancelled(true);
        } else {
            //Need to cancel the event anyways? Or should non-team people interact with teamed people?
            event.setCancelled(true);
        }
    }
}
