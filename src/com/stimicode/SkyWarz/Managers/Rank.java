package com.stimicode.SkyWarz.Managers;

import com.stimicode.SkyWarz.Account.CoreAccount;
import com.stimicode.SkyWarz.Util.C;
import com.stimicode.SkyWarz.Util.F;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 * Created by Derrick on 6/16/2015.
 */
public enum Rank {
                      Owner("Owner", C.B_D_Purple),
              Administrator("Admin", C.B_Red     ),
                  Moderator("Mod"  , C.B_Yellow  ),
                    Default(""     , ""          );

    String displayName;
    String format;

    public static ArrayList<String> ranks = new ArrayList<>();

    Rank(String name, String form) {
        this.displayName = name;
        this.format = form;
    }

    public String get(boolean bold, boolean uppercase) {
        String name = this.displayName;
        if (uppercase) {
            name = this.displayName.toUpperCase();
        }
        if (bold)
            return this.format + ChatColor.BOLD + name;
        return this.format + name;
    }

    public static void init() {
        ranks.add("Default, ");
        ranks.add("Moderator, ");
        ranks.add("Administrator, ");
        ranks.add("Owner");
    }

    /**
     *
     * @param rank t
     *
     * @return The Name of the Rank in String Forum.
     */
    public static String getRankName(Rank rank) {
        switch (rank) {
            case Default:
                return "default";
            case Moderator:
                return "moderator";
            case Administrator:
                return "administrator";
            case Owner:
                return "owner";
            default:
                break;
        }
        return null;
    }

    /**
     * Static Method that takes a string to return a Rank Variable.
     * @param string The name of a rank in string forum.
     * @return The corresponding rank to the given string.
     */
    public static Rank getRank(String string) {
        switch (string) {
            case "default":
                return Rank.Default;
            case "moderator":
                return Rank.Moderator;
            case "administrator":
                return Rank.Administrator;
            case "owner":
                return Rank.Owner;
            default:
                break;
        }
        return null;
    }

    public boolean has(CoreAccount player, Rank rank, boolean inform) {
        if (compareTo(rank) <= 0) {
            return true;
        }

        if (inform) {
            MessageManager.send(player, F.info("You do not have the required rank. (" + rank + ")"));
        }
        return false;
    }

    public boolean has(Player player, Rank rank, boolean inform) {
        if (compareTo(rank) <= 0) {
            return true;
        }

        if (inform) {
            MessageManager.send(player, F.info("You do not have the required rank. (" + rank + ")"));
        }
        return false;
    }
}
