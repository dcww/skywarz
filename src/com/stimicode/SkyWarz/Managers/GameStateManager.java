package com.stimicode.SkyWarz.Managers;

/**
 * Created by Derrick on 7/27/2015.
 */
public enum GameStateManager {
    Lobby(true),
    PreGame(false),
    InGame(false),
    PostGame(false);

    GameStateManager(boolean joinable) {
        canJoin = joinable;
    }

    private boolean canJoin;

    private static GameStateManager currentState;

    public boolean canJoin() {
        return canJoin;
    }

    public static void setState(GameStateManager state) {
        currentState = state;
    }

    public static boolean isState(GameStateManager state) {
        return currentState == state;
    }

    public static GameStateManager getState() {
        return currentState;
    }

    public static GameStateManager getGameState(String string) {
        switch (string) {
            case "lobby":
                return GameStateManager.Lobby;
            case "pregame":
                return GameStateManager.PreGame;
            case "ingame":
                return GameStateManager.InGame;
            case "postgame":
                return GameStateManager.PostGame;
            default:
                break;
        }
        return null;
    }
}
