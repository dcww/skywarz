package com.stimicode.SkyWarz.Object;

import com.stimicode.SkyWarz.Account.CoreAccount;
import org.bukkit.Location;

/**
 * Created by Derrick on 7/28/2015.
 */
public class SpawnLocation {

    public String name;
    public Location location;
    public boolean occupied;
    public CoreAccount account;

    public SpawnLocation(Location loc, boolean occ) {
        //this.location = new Location(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), loc.getYaw(), loc.getPitch());
        this.location = loc;
        this.occupied = occ;
    }

    public SpawnLocation(String name, Location loc, boolean occ) {
        this.name = name;
        //this.location = new Location(loc.getWorld(), loc.getBlockX()+0.5, loc.getBlockY(), loc.getBlockZ()+0.5, loc.getYaw(), loc.getPitch());
        this.location = loc;
        this.occupied = occ;
    }

    public SpawnLocation() {}

    public void setLocation(Location loc) {
        this.location = loc;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setOccupied(boolean occ) {
        this.occupied = occ;
    }

    public boolean isOccupied() {
        return this.occupied;
    }

    public void setAccount(CoreAccount account) {
        this.account = account;
    }

    public CoreAccount getAccount() { return this.account; }

    public void setName(String name) { this.name = name; }

    public String getName() { return this.name; }
}
