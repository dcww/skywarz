package com.stimicode.SkyWarz.Object;

import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.HashMap;

/**
 * Created by Derrick on 8/8/2015.
 */
public class MultiWorld {

    private String name;
    private World world;
    private boolean isGameWorld = false;
    public HashMap<String, SpawnLocation> spawnLocations = new HashMap<>();

    private FileConfiguration config;

    public MultiWorld(String name, World world) {
        this.name = name;
        this.world = world;
    }

    public void loadConfig() {
        try {
           config = Manager.settingsManager.loadConfigurationFile(this.getName()+".yml");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadSpawns() {
        if (!(config.contains("Spawns"))) {
            Log.info("There are currently no spawns for "+config.getName()+"! Skipping Spawn Loading.");
            return;
        }
        ConfigurationSection configSection = config.getConfigurationSection("Spawns");
        for (String locationName : configSection.getKeys(false)) {
            World world = Bukkit.getWorld(this.getName());
            int x = configSection.getInt(locationName + ".x");
            int y = configSection.getInt(locationName + ".y");
            int z = configSection.getInt(locationName + ".z");
            Double yawTemp = (Double) configSection.get(locationName + ".yaw");
            Double pitchTemp = (Double) configSection.get(locationName + ".pitch");
            float yaw = yawTemp.floatValue();
            float pitch = pitchTemp.floatValue();
            Location loc = new Location(world, x + 0.5, y, z + 0.5, yaw, pitch);
            SpawnLocation spawnLocation = new SpawnLocation(loc, false);
            spawnLocations.put(locationName, spawnLocation);
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public HashMap<String, SpawnLocation> getSpawnLocations() {
        return spawnLocations;
    }

    public void addSpawnLocation(String pos, SpawnLocation loc) {
        spawnLocations.put(pos, loc);
    }

    public void setSpawnLocations(HashMap<String, SpawnLocation> spawnLocations) {
        this.spawnLocations = spawnLocations;
    }

    public FileConfiguration getConfig() { return this.config; }

    public boolean isGameWorld() {
        return isGameWorld;
    }

    public void setIsGameWorld(boolean isGameWorld) {
        this.isGameWorld = isGameWorld;
    }
}
