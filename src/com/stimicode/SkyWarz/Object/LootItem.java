package com.stimicode.SkyWarz.Object;

import org.bukkit.Material;

/**
 * Created by Derrick on 7/28/2015.
 */
public class LootItem {

    public Material material;
    public byte data;
    public int chance; //0% - 100%
    public int amount;

    public LootItem(Material material, int data, int chance, int amount) {
        this.material = material;
        this.data = (byte) data;
        this.chance = chance;
        this.amount = amount;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public byte getData() {
        return data;
    }

    public void setData(byte data) {
        this.data = data;
    }

    public int getChance() {
        return chance;
    }

    public void setChance(int chance) {
        this.chance = chance;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
