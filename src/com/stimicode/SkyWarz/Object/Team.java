package com.stimicode.SkyWarz.Object;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * Created by Derrick on 8/6/2015.
 */
public class Team {

    private String teamName;
    private Color teamColor;

    private HashSet<UUID> teamPlayers = new HashSet<>();

    public Team(String name, Color color) {
        this.teamName = name;
        this.teamColor = color;
    }

    public String getTeamName() {
        return this.teamName;
    }

    public Color getTeamColor() {
        return this.teamColor;
    }

    public void addPlayer(Player player) {
        this.teamPlayers.add(player.getUniqueId());
    }

    public void removePlayer(Player player) {
        this.teamPlayers.remove(player.getUniqueId());
    }

    public Collection<Player> getTeamPlayers() {
        ArrayList<Player> temp = new ArrayList<>();
        for (UUID uid : this.teamPlayers) {
            temp.add(Bukkit.getPlayer(uid));
        }
        return temp;
    }

    public boolean hasPlayer(Player player) {
        return this.teamPlayers.contains(player.getUniqueId());
    }
}
