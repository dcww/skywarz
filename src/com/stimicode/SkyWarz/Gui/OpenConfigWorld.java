package com.stimicode.SkyWarz.Gui;

import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Tasks.OpenInventoryTask;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 9/16/2015.
 */
public class OpenConfigWorld implements GuiAction {

    public void open(Player player, ItemStack stack) {
        int i = 0;

        Inventory guiInventory = GuiItem.createGUIInventory(1 * 9, "Admin World Configuration");

        ItemStack dayCycleConfig = GuiItem.build("Toggle Day Cycle", Material.WRITTEN_BOOK, 0, "Toggles Day Cycle.");
        try {
            if (Manager.settingsManager.getBoolean("Worlds." + GuiItem.getActionData(stack, "world") + ".day-cycle")) {
                dayCycleConfig = GuiItem.addLore(dayCycleConfig, C.Green + "Enabled");
            } else {
                dayCycleConfig = GuiItem.addLore(dayCycleConfig, C.Red + "Disabled");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        dayCycleConfig = GuiItem.setActionClass(dayCycleConfig, ToggleDayCycle.class);
        dayCycleConfig = GuiItem.setActionData(dayCycleConfig, "world", GuiItem.getActionData(stack, "world"));
        guiInventory.setItem(i, dayCycleConfig);
        i++;

        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        open((Player)event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        open(event.getPlayer(), stack);
    }
}
