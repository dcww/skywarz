package com.stimicode.SkyWarz.Gui;

import com.stimicode.SkyWarz.Managers.Gui.CommandButton;
import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Object.MultiWorld;
import com.stimicode.SkyWarz.Tasks.OpenInventoryTask;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 9/12/2015.
 */
public class OpenWorldList implements GuiAction {

    public void open(Player player, ItemStack stack) {
        int i = 0;

        Inventory guiInventory = GuiItem.createGUIInventory(9, "Admin World Teleport");
        for (MultiWorld multiWorld : Manager.worldManager.worlds.values()) {
            ItemStack itemStack;
            itemStack = GuiItem.build(C.Gold + "Teleport To " + C.Green + multiWorld.getName(), Material.BOOK, 0, "Teleports you to the given worlds spawn location.");
            itemStack = GuiItem.setActionClass(itemStack, CommandButton.class);
            itemStack = GuiItem.setActionData(itemStack, "className", "com.stimicode.SkyWarz.Command.Admin.AdminWorldCommand");
            itemStack = GuiItem.setActionData(itemStack, "command", "teleport " + multiWorld.getName());
            guiInventory.setItem(i, itemStack);
            i++;
        }
        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        open((Player)event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        open(event.getPlayer(), stack);
    }
}
