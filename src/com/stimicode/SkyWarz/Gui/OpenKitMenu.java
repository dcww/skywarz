package com.stimicode.SkyWarz.Gui;

import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Tasks.OpenInventoryTask;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 9/13/2015.
 */
public class OpenKitMenu implements GuiAction {

    public void open(Player player, ItemStack stack) {
        int i = 0;

        Inventory guiInventory = GuiItem.createGUIInventory(9, "Admin Commands");

        ItemStack adminWorld = GuiItem.build("World Commands", Material.LAVA_BUCKET, 0, "World Related Commands");
        adminWorld = GuiItem.setActionClass(adminWorld, OpenAdminWorldCommandMenu.class);
        adminWorld = GuiItem.setActionData(adminWorld, "world", player.getWorld().getName());
        guiInventory.setItem(i, adminWorld);
        i++;

        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        open((Player)event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        open(event.getPlayer(), stack);
    }
}
