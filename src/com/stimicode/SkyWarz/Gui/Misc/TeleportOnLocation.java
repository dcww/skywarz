package com.stimicode.SkyWarz.Gui.Misc;

import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Util.BlockUtility;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 8/5/2016.
 */
public class TeleportOnLocation implements GuiAction {

    public void handle(Player player, ItemStack stack) {
        player.teleport(BlockUtility.stringToLocation(GuiItem.getActionData(stack, "location")));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        handle((Player) event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        handle(event.getPlayer(), stack);
    }
}