package com.stimicode.SkyWarz.Gui;

import com.stimicode.SkyWarz.Main.SkyWarz;
import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.Manager;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.io.IOException;

/**
 * Created by Derrick on 9/16/2015.
 */
public class ToggleDayCycle implements GuiAction {

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        World world = Bukkit.getWorld(GuiItem.getActionData(stack, "world"));
        boolean temp = false;

        if (world.getGameRuleValue("doDaylightCycle") == "true") {
            temp = true;
        } else if (world.getGameRuleValue("doDaylightCycle") == "false") {
            temp = false;
        }

        world.setGameRuleValue("doDaylightCycle", Boolean.toString(!(temp)));
        Manager.settingsManager.config.set("Worlds." + GuiItem.getActionData(stack, "world") + ".day-cycle", !(temp));
        try {
            Manager.settingsManager.config.save(new File(SkyWarz.getPlugin().getDataFolder().getPath() + "/config.yml"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        event.getWhoClicked().closeInventory();
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
    }
}
