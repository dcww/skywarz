package com.stimicode.SkyWarz.Gui.Game;

import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Object.SpawnLocation;
import com.stimicode.SkyWarz.Tasks.OpenInventoryTask;
import com.stimicode.SkyWarz.Util.BlockUtility;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 8/5/2016.
 */
public class OpenGameSpawnManagementMenu implements GuiAction {

    public void open(Player player, ItemStack stack) {
        Inventory guiInventory = GuiItem.createGUIInventory(3 * 9, "Admin Game Spawn Management");

        ItemStack stack1 = GuiItem.build("Information", Material.PAPER, 0, "Current Number Of Spawns: "+Manager.worldManager.worlds.get(player.getWorld().getName()).spawnLocations.size());
        guiInventory.addItem(stack1);

        ItemStack stack2;
        int temp = 0;
        int loc = 9;
        for (SpawnLocation location : Manager.worldManager.worlds.get(player.getWorld().getName()).getSpawnLocations().values()) {
            temp++;
            stack2 = GuiItem.build("Spawn Point "+temp, Material.BED, 0, C.Gold+"<Click To Teleport>",C.Gold+"<Shift Click To Set>");
            stack2 = GuiItem.setActionClass(stack2, HandleSpawnManagement.class);
            stack2 = GuiItem.setActionData(stack2, "location", BlockUtility.locationToString(location.getLocation()));
            stack2 = GuiItem.setActionData(stack2, "number", temp+"");
            guiInventory.setItem(loc, stack2);
            loc++;
        }

        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        open((Player) event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        open(event.getPlayer(), stack);
    }
}
