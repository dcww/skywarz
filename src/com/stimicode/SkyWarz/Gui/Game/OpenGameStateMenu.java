package com.stimicode.SkyWarz.Gui.Game;

import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 8/3/2016.
 */
public class OpenGameStateMenu implements GuiAction {

    public void open(Player player, ItemStack stack) {
        Inventory guiInventory = GuiItem.createGUIInventory(2 * 9, "Admin Game Commands");

        ItemStack stack1 = GuiItem.build("Spawn Management", Material.EYE_OF_ENDER, 0, "Opens the Spawn Management for this game world.", C.Gold+"<Click To Open>");
        stack1 = GuiItem.setActionClass(stack1, OpenGameSpawnManagementMenu.class);
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        open((Player)event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        open(event.getPlayer(), stack);
    }
}