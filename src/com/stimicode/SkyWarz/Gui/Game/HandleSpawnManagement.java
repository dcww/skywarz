package com.stimicode.SkyWarz.Gui.Game;

import com.stimicode.SkyWarz.Game.Game;
import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Object.SpawnLocation;
import com.stimicode.SkyWarz.Util.BlockUtility;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 8/13/2016.
 */
public class HandleSpawnManagement implements GuiAction {

    public void handleSpawnSet(Player player, ItemStack stack) {
        try {
            Game.setSpawn(GuiItem.getActionData(stack, "number"), new SpawnLocation(player.getLocation(), false));
            MessageManager.send(player, "Set spawn point "+GuiItem.getActionData(stack, "number"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void handleSpawnTeleport(Player player, ItemStack stack) {
        player.teleport(BlockUtility.stringToLocation(GuiItem.getActionData(stack, "location")));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        if (event.getClick().isShiftClick()) {
            handleSpawnSet((Player) event.getWhoClicked(), stack);
        } else {
            handleSpawnTeleport((Player) event.getWhoClicked(), stack);
        }
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {

    }
}
