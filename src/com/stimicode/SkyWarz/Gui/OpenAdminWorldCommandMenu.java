package com.stimicode.SkyWarz.Gui;

import com.stimicode.SkyWarz.Managers.Gui.CommandButton;
import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Tasks.OpenInventoryTask;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 9/12/2015.
 */
public class OpenAdminWorldCommandMenu implements GuiAction {

    public void open(Player player, ItemStack stack) {
        int i = 10;

        Inventory guiInventory = GuiItem.createGUIInventory(2 * 9, "Admin World Commands [" + GuiItem.getActionData(stack, "world") + "]");

        ItemStack genericInfo = GuiItem.build("Generic World Commands", Material.PAPER, 0 , "World commands that are not world specific.");
        guiInventory.setItem(0, genericInfo);

        ItemStack specificInfo = GuiItem.build("World Specific Commands", Material.PAPER, 0, "World commands that are world specific.", "Effects the currently occupied world.");
        guiInventory.setItem(9, specificInfo);

        ItemStack teleportWorld = GuiItem.build("Teleport", Material.ENDER_PEARL, 0, "Teleports to a world.");
        teleportWorld = GuiItem.setActionClass(teleportWorld, OpenWorldList.class);
        guiInventory.setItem(1, teleportWorld);

        ItemStack listWorld = GuiItem.build("List", Material.WRITTEN_BOOK, 0, "Lists all of the currently loaded worlds.");
        listWorld = GuiItem.setActionClass(listWorld, CommandButton.class);
        listWorld = GuiItem.setActionData(listWorld, "className", "com.stimicode.SkyWarz.Command.Admin.AdminWorldCommand");
        listWorld = GuiItem.setActionData(listWorld, "command", "list");
        guiInventory.setItem(2, listWorld);

        ItemStack saveWorld = GuiItem.build("Save World", Material.BED, 0, "Saves '" + GuiItem.getActionData(stack, "world") + "'!");
        saveWorld = GuiItem.setActionClass(saveWorld, CommandButton.class);
        saveWorld = GuiItem.setActionData(saveWorld, "className", "com.stimicode.SkyWarz.Command.Admin.AdminWorldCommand");
        saveWorld = GuiItem.setActionData(saveWorld, "command", "save " + GuiItem.getActionData(stack, "world"));
        guiInventory.setItem(i, saveWorld);
        i++;

        ItemStack resetWorld = GuiItem.build("Reset World", Material.LAVA_BUCKET, 0, "Resets '" + GuiItem.getActionData(stack, "world") + "' from template!");
        resetWorld = GuiItem.setActionClass(resetWorld, CommandButton.class);
        resetWorld = GuiItem.setActionData(resetWorld, "className", "com.stimicode.SkyWarz.Command.Admin.AdminWorldCommand");
        resetWorld = GuiItem.setActionData(resetWorld, "command", "reset " + GuiItem.getActionData(stack, "world"));
        guiInventory.setItem(i, resetWorld);
        i++;

        ItemStack spawnsWorld = GuiItem.build("List World Spawns", Material.WRITTEN_BOOK, 0, "Lists the spawns points of '" + GuiItem.getActionData(stack, "world") + "'!", "(Only applies to Game Worlds)");
        spawnsWorld = GuiItem.setActionClass(spawnsWorld, CommandButton.class);
        spawnsWorld = GuiItem.setActionData(spawnsWorld, "className", "com.stimicode.SkyWarz.Command.Admin.AdminWorldCommand");
        spawnsWorld = GuiItem.setActionData(spawnsWorld, "command", "listspawns " + GuiItem.getActionData(stack, "world"));
        guiInventory.setItem(i, spawnsWorld);
        i++;

        ItemStack configWorld = GuiItem.build("Configure World", Material.BOOK_AND_QUILL, 0, "Configure '" + GuiItem.getActionData(stack, "world") + "'!");
        configWorld = GuiItem.setActionClass(configWorld, OpenConfigWorld.class);
        configWorld = GuiItem.setActionData(configWorld, "world", GuiItem.getActionData(stack, "world"));
        guiInventory.setItem(i, configWorld);
        i++;

        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        open((Player)event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        open(event.getPlayer(), stack);
    }
}
