package com.stimicode.SkyWarz.Gui;

import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.Rank;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Object.LootItem;
import com.stimicode.SkyWarz.Tasks.OpenInventoryTask;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Created by Derrick on 8/20/2016.
 */
public class OpenChestLoot implements GuiAction {

    public void open(Player player, ItemStack stack) {
        int i = 0;

        Inventory guiInventory = GuiItem.createGUIInventory(1 * 9, "Admin Commands");

        ItemStack temp;
        for (LootItem item : Manager.settingsManager.lootItemMap.values()) {
            ItemMeta temps = Bukkit.getItemFactory().getItemMeta(item.getMaterial());
            temp = GuiItem.build(temps.getDisplayName(), item.getMaterial(), 0, C.Gold+"<Click To Edit>");
            temp = GuiItem.setActionClass(temp, OpenEditLootItem.class);
            guiInventory.addItem(temp);
        }
        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        if (!Manager.accountManager.getAccount((Player) event.getWhoClicked()).hasRank(Rank.Administrator, false)) {
            MessageManager.sendError(event.getWhoClicked(), "Do not have permission to do that.");
            return;
        }
        open((Player)event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        if (!Manager.accountManager.getAccount(event.getPlayer()).hasRank(Rank.Administrator, false)) {
            MessageManager.sendError(event.getPlayer(), "Do not have permission to do that.");
            return;
        }
        open(event.getPlayer(), stack);
    }
}
