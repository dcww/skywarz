package com.stimicode.SkyWarz.Gui;

import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Tasks.OpenInventoryTask;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 10/18/2015.
 */
public class OpenChestSearch implements GuiAction {

    public void open(Player player, ItemStack stack) {
        int i = 0;

        Inventory guiInventory = GuiItem.createGUIInventory(1 * 9, "Admin Commands");

        ItemStack adminGame = GuiItem.build("Game Commands", Material.BEDROCK, 0, "Game Related Commands");
        adminGame = GuiItem.setActionClass(adminGame, OpenAdminGameCommandMenu.class);
        guiInventory.setItem(i, adminGame);
        i++;

        ItemStack adminWorld = GuiItem.build("World Commands", Material.LAVA_BUCKET, 0, "World Related Commands");
        adminWorld = GuiItem.setActionClass(adminWorld, OpenAdminWorldCommandMenu.class);
        adminWorld = GuiItem.setActionData(adminWorld, "world", player.getWorld().getName());
        guiInventory.setItem(i, adminWorld);
        i++;

        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    public void open(Player player, ItemStack stack, Material mat) {
        int i = 0;

        Inventory guiInventory = GuiItem.createGUIInventory(1 * 9, "Admin Commands");

        for (Chunk chunk : player.getWorld().getLoadedChunks()) {
            for (BlockState block : chunk.getTileEntities()) {
                if (block.getType().equals(Material.CHEST)) {
                    Block bloc = player.getWorld().getBlockAt(block.getX(), block.getY(), block.getZ());
                    Chest chest = (Chest) block;
                    if (chest.getInventory().contains(Material.STONE)) {
                        //MessageManager.send(player, bloc.getLocation().toString());
                        ItemStack iteml;

                    }
                }
            }
        }

        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        open((Player)event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        open(event.getPlayer(), stack);
    }
}
