package com.stimicode.SkyWarz.Gui;

import com.stimicode.SkyWarz.Managers.Gui.GuiAction;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.Rank;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Tasks.OpenInventoryTask;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 * Created by Derrick on 9/12/2015.
 */
public class OpenAdminCommandMenu implements GuiAction {

    public void open(Player player, ItemStack stack) {
        int i = 0;

        Inventory guiInventory = GuiItem.createGUIInventory(1 * 9, "Admin Commands");

        ItemStack adminGame = GuiItem.build("Game Commands", Material.BEDROCK, 0, "Game Related Commands");
        adminGame = GuiItem.setActionClass(adminGame, OpenAdminGameCommandMenu.class);
        guiInventory.setItem(i, adminGame);
        i++;

        ItemStack adminWorld = GuiItem.build("World Commands", Material.LAVA_BUCKET, 0, "World Related Commands");
            adminWorld = GuiItem.setActionClass(adminWorld, OpenAdminWorldCommandMenu.class);
            adminWorld = GuiItem.setActionData(adminWorld, "world", player.getWorld().getName());
        guiInventory.setItem(i, adminWorld);
        i++;

        TaskManager.syncTask(new OpenInventoryTask(player, guiInventory));
    }

    @Override
    public void performAction(InventoryClickEvent event, ItemStack stack) {
        if (!Manager.accountManager.getAccount((Player) event.getWhoClicked()).hasRank(Rank.Administrator, false)) {
            MessageManager.sendError(event.getWhoClicked(), "Do not have permission to do that.");
            return;
        }
        open((Player)event.getWhoClicked(), stack);
    }

    @Override
    public void performAction(PlayerInteractEvent event, ItemStack stack) {
        if (!Manager.accountManager.getAccount(event.getPlayer()).hasRank(Rank.Administrator, false)) {
            MessageManager.sendError(event.getPlayer(), "Do not have permission to do that.");
            return;
        }
        open(event.getPlayer(), stack);
    }
}
