package com.stimicode.SkyWarz.Util;

import org.bukkit.Location;

/**
 * Created by Derrick on 6/16/2015.
 */
public class F {
    /**
     * Class Name F = Format
     */

    public static String jqm(String head, String body) {
        return C.B_Aqua+head+" >>> "+ C.B_Green+body;
    }

    public static String info(String body) {
        return C.B_Yellow+"[INFO] "+ C.B_Green+body;
    }

    public static String locationFormatter(Location loc) {
        return C.Green+"World: "+C.Aqua+loc.getWorld().getName()+C.Green+", X: "+C.Aqua+loc.getBlockX()+C.Green+", Y: "+C.Aqua+loc.getBlockY()+C.Green+", Z: "+C.Aqua+loc.getBlockZ() +
                C.Green;
    }
}
