package com.stimicode.SkyWarz.Util;

import org.bukkit.Location;
import org.bukkit.Material;

import java.util.Date;
import java.util.Random;

/**
 * Created by Derrick on 7/1/2015.
 */
public class BlockRestoreData {

    //private Location location;
    private int x, y, z;
    private Material material;
    private byte data;
    private String world;
    private Date timeChanged;
    private int timeDelay;

    public BlockRestoreData(Location loc, Material mat, byte byt) {
        //this.location = loc;
        this.x = loc.getBlockX();
        this.y = loc.getBlockY();
        this.z = loc.getBlockZ();
        this.material = mat;
        this.data = byt;
        this.world = loc.getWorld().getName();
    }

    public BlockRestoreData(Location loc, Material mat, byte byt, Date date) {
        //this.location = loc;
        this.x = loc.getBlockX();
        this.y = loc.getBlockY();
        this.z = loc.getBlockZ();
        this.material = mat;
        this.data = byt;
        this.world = loc.getWorld().getName();
        this.timeChanged = date;
        Random rand = new Random();
        this.timeDelay = rand.nextInt((5000 - 3000) + 1) + 3000;
    }

    public int getTimeDelay() {
        return this.timeDelay;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public Material getMaterial() {
        return material;
    }

    public byte getData() {
        return data;
    }

    public String getWorld() {
        return world;
    }

    public Date getTimeChanged() {
        return timeChanged;
    }
}
