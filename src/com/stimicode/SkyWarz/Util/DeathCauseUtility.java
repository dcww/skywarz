package com.stimicode.SkyWarz.Util;

import org.bukkit.event.entity.EntityDamageEvent;

/**
 * Created by Derrick on 8/8/2015.
 */
public class DeathCauseUtility {

    public static String getCauseMessage(EntityDamageEvent.DamageCause cause) {
        switch (cause) {
            case SUFFOCATION:
                return "suffocating in a block";
            case FALL:
                return "falling to their death";
            case FIRE:
                return "burning to death";
            case FIRE_TICK:
                return "burning to death";
            case LAVA:
                return "falling into lava";
            case DROWNING:
                return "drowning";
            case BLOCK_EXPLOSION:
                return "getting blown up by tnt";
            case ENTITY_EXPLOSION:
                return "getting blown up by";
            case VOID:
                return "falling into the abyss";
            case LIGHTNING:
                return "getting struck by lightning";
            case SUICIDE:
                return "killing themselves";
            case STARVATION:
                return "starving to death";
            case FALLING_BLOCK:
                return "getting smashed";
            default:
                return cause.toString();
        }
    }
}
