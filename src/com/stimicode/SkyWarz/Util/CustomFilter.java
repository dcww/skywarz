package com.stimicode.SkyWarz.Util;

import java.util.logging.Filter;
import java.util.logging.LogRecord;

/**
 * Created by Derrick on 9/11/2015.
 */
public class CustomFilter implements Filter {

    @Override
    public boolean isLoggable(LogRecord logRecord) {
        if (logRecord.getMessage().contains("")) {
            return false;
        } else {
            return true;
        }
    }
}
