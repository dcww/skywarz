package com.stimicode.SkyWarz.Util;

import com.stimicode.SkyWarz.Main.Log;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;

/**
 * Created by Derrick on 6/25/2015.
 */
public class EventUtility {

    public static Player getShooterProjectileHitEvent(ProjectileHitEvent event) {
        Entity entity = (Entity) event.getEntity().getShooter();
        if (entity instanceof Player) {
            return (Player) entity;
        }
        return null;
    }

    @SuppressWarnings("deprecation")
    public static boolean isPlayerProjectileHitEvent(ProjectileHitEvent event) {
        Entity entity = (Entity) event.getEntity().getShooter();
        Log.info("Entity " + entity.getName() + " " + entity.getCustomName() + " " + entity.getType().getName() + " " + entity.getType());
        if (entity instanceof Player) {
            Log.info("Returned true");
            return true;
        }
        return false;
    }
}
