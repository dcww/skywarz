package com.stimicode.SkyWarz.Util;

import com.stimicode.SkyWarz.Main.Log;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.HashMap;
import java.util.HashSet;

/**
 * Created by Derrick on 6/19/2015.
 */
public class BlockUtility {

    public static HashSet<Material> usableBlocks = new HashSet<>();
    public static HashSet<Material> solidBlocks = new HashSet<>();

    public static boolean solid(Block block) {
        if (block == null) {
            return false;
        }
        return solid(block.getType());
    }

    public static boolean solid(Material block) {
        if (solidBlocks.isEmpty()) {
            solidBlocks.add(Material.AIR);
            solidBlocks.add(Material.SAPLING);
            solidBlocks.add(Material.WATER);
            solidBlocks.add(Material.STATIONARY_WATER);
            solidBlocks.add(Material.LAVA);
            solidBlocks.add(Material.STATIONARY_LAVA);
            solidBlocks.add(Material.RAILS);
            solidBlocks.add(Material.ACTIVATOR_RAIL);
            solidBlocks.add(Material.DETECTOR_RAIL);
            solidBlocks.add(Material.POWERED_RAIL);
            solidBlocks.add(Material.BROWN_MUSHROOM);
            solidBlocks.add(Material.RED_MUSHROOM);
            solidBlocks.add(Material.DEAD_BUSH);
            solidBlocks.add(Material.LONG_GRASS);
            solidBlocks.add(Material.YELLOW_FLOWER);
            solidBlocks.add(Material.RED_ROSE);
            solidBlocks.add(Material.TORCH);
            solidBlocks.add(Material.ACACIA_STAIRS);
            solidBlocks.add(Material.BIRCH_WOOD_STAIRS);
            solidBlocks.add(Material.BRICK_STAIRS);
            solidBlocks.add(Material.COBBLESTONE_STAIRS);
            solidBlocks.add(Material.DARK_OAK_STAIRS);
            solidBlocks.add(Material.JUNGLE_WOOD_STAIRS);
            solidBlocks.add(Material.NETHER_BRICK_STAIRS);
            solidBlocks.add(Material.QUARTZ_STAIRS);
            solidBlocks.add(Material.RED_SANDSTONE_STAIRS);
            solidBlocks.add(Material.SANDSTONE_STAIRS);
            solidBlocks.add(Material.SMOOTH_STAIRS);
            solidBlocks.add(Material.SPRUCE_WOOD_STAIRS);
            solidBlocks.add(Material.WOOD_STAIRS);
            solidBlocks.add(Material.REDSTONE);
            solidBlocks.add(Material.REDSTONE_TORCH_ON);
        }
        return solidBlocks.contains(block);
    }

    public static boolean usable(Block block) {
        if (block == null) {
            return false;
        }
        return usable(block.getType());
    }

    public static boolean usable(Material block) {
        if (usableBlocks.isEmpty()) {
            usableBlocks.add(Material.DISPENSER);
            usableBlocks.add(Material.BED_BLOCK);
            usableBlocks.add(Material.CHEST);
            usableBlocks.add(Material.ENDER_CHEST);
            usableBlocks.add(Material.TRAPPED_CHEST);
            usableBlocks.add(Material.WORKBENCH);
            usableBlocks.add(Material.FURNACE);
            usableBlocks.add(Material.BURNING_FURNACE);
            usableBlocks.add(Material.ACACIA_DOOR);
            usableBlocks.add(Material.BIRCH_DOOR);
            usableBlocks.add(Material.DARK_OAK_DOOR);
            usableBlocks.add(Material.IRON_DOOR);
            usableBlocks.add(Material.JUNGLE_DOOR);
            usableBlocks.add(Material.SPRUCE_DOOR);
            usableBlocks.add(Material.TRAP_DOOR);
            usableBlocks.add(Material.WOODEN_DOOR);
            usableBlocks.add(Material.WOOD_DOOR);
            usableBlocks.add(Material.LEVER);
            usableBlocks.add(Material.STONE_BUTTON);
            usableBlocks.add(Material.WOOD_BUTTON);
            usableBlocks.add(Material.JUKEBOX);
            usableBlocks.add(Material.REDSTONE_COMPARATOR);
            usableBlocks.add(Material.REDSTONE_COMPARATOR_OFF);
            usableBlocks.add(Material.REDSTONE_COMPARATOR_ON);
            usableBlocks.add(Material.ENCHANTMENT_TABLE);
            usableBlocks.add(Material.BREWING_STAND);
            usableBlocks.add(Material.ANVIL);
            usableBlocks.add(Material.HOPPER);
            usableBlocks.add(Material.MINECART);
            usableBlocks.add(Material.COMMAND_MINECART);
            usableBlocks.add(Material.HOPPER_MINECART);
            usableBlocks.add(Material.POWERED_MINECART);
            usableBlocks.add(Material.STORAGE_MINECART);
            usableBlocks.add(Material.DROPPER);
            usableBlocks.add(Material.ACACIA_FENCE_GATE);
            usableBlocks.add(Material.BIRCH_FENCE_GATE);
            usableBlocks.add(Material.DARK_OAK_FENCE_GATE);
            usableBlocks.add(Material.FENCE_GATE);
            usableBlocks.add(Material.JUNGLE_FENCE_GATE);
            usableBlocks.add(Material.SPRUCE_FENCE_GATE);
            usableBlocks.add(Material.ARMOR_STAND);
        }
        return usableBlocks.contains(block);
    }

    public static HashMap<Block, Double> getInRadius(Location loc, double radius) {
        return getInRadius(loc, radius, 260.0D);
    }

    public static HashMap<Block, Double> getInRadius(Location loc, double radius, double heightLimit) {
        HashMap<Block, Double> blockList = new HashMap<>();
        int iR = (int) radius + 1;

        for (int x = -iR; x <= iR; x++) {
            for (int z = -iR; z <= iR; z++) {
                for (int y = -iR; y <= iR; y++) {
                    if (Math.abs(y) <= heightLimit) {
                        Block curBlock = loc.getWorld().getBlockAt((int) (loc.getX() + x), (int) (loc.getY() + y), (int) (loc.getZ() + z));
                        double offset = MathUtility.offset(loc, curBlock.getLocation().add(0.5D, 0.5D, 0.5D));
                        if (offset <= radius) {
                            blockList.put(curBlock, 1.0D - offset / radius);
                        }
                    }
                }
            }
        }
        return blockList;
    }

    public static String locationToString(Location loc) {
        return loc.getX()+","+loc.getY()+","+loc.getZ()+","+loc.getWorld().getName()+","+loc.getPitch()+","+loc.getYaw();
    }

    public static Location stringToLocation(String string) {
        String[] temp = string.split(",");
        return new Location(Bukkit.getWorld(temp[3]), Double.valueOf(temp[0]), Double.valueOf(temp[1]), Double.valueOf(temp[2]), Float.valueOf(temp[5]), Float.valueOf(temp[4]));
    }
}