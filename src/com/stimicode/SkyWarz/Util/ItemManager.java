package com.stimicode.SkyWarz.Util;

import org.bukkit.ChunkSnapshot;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.Chest;
import org.bukkit.material.MaterialData;

import java.util.LinkedList;
import java.util.Random;

/**
 * Created by Derrick on 6/17/2015.
 */
public class ItemManager {
    @SuppressWarnings("deprecation")
    public static ItemStack createItemStack(int typeId, int amount, short damage) {
        return new ItemStack(typeId, amount, damage);
    }

    public static ItemStack createItemStack(int typeId, int amount) {
        return createItemStack(typeId, amount, (short)0);
    }

    public static ItemStack createItemStack(Material typeMaterial, int amount, short damage) {
        return new ItemStack(typeMaterial, amount, damage);
    }

    @SuppressWarnings("deprecation")
    public static MaterialData getMaterialData(int type_id, int data) {
        return new MaterialData(type_id, (byte)data);
    }

    @SuppressWarnings("deprecation")
    public static Enchantment getEnchantById(int id) {
        return Enchantment.getById(id);
    }

    @SuppressWarnings("deprecation")
    public static int getId(Material material) {
        return material.getId();
    }

    @SuppressWarnings("deprecation")
    public static int getId(Enchantment e) {
        return e.getId();
    }

    @SuppressWarnings("deprecation")
    public static int getId(ItemStack stack) {
        return stack.getTypeId();
    }

    @SuppressWarnings("deprecation")
    public static int getId(Block block) {
        return block.getTypeId();
    }

    @SuppressWarnings("deprecation")
    public static void setTypeId(Block block, int typeId) {
        block.setTypeId(typeId);
    }

    @SuppressWarnings("deprecation")
    public static void setTypeId(BlockState block, int typeId) {
        block.setTypeId(typeId);
    }

    @SuppressWarnings("deprecation")
    public static byte getData(Block block) {
        return block.getData();
    }

    public static short getData(ItemStack stack) {
        return stack.getDurability();
    }

    @SuppressWarnings("deprecation")
    public static byte getData(MaterialData data) {
        return data.getData();
    }

    @SuppressWarnings("deprecation")
    public static byte getData(BlockState state) {
        return state.getRawData();
    }

    @SuppressWarnings("deprecation")
    public static void setData(Block block, int data) {
        block.setData((byte)data);
    }

    @SuppressWarnings("deprecation")
    public static void setData(Block block, int data, boolean update) {
        block.setData((byte) data, update);
    }

    @SuppressWarnings("deprecation")
    public static Material getMaterial(int material) {
        return Material.getMaterial(material);
    }

    @SuppressWarnings("deprecation")
    public static int getBlockTypeId(ChunkSnapshot snapshot, int x, int y, int z) {
        return snapshot.getBlockTypeId(x, y, z);
    }

    @SuppressWarnings("deprecation")
    public static int getBlockData(ChunkSnapshot snapshot, int x, int y, int z) {
        return snapshot.getBlockData(x, y, z);
    }

    @SuppressWarnings("deprecation")
    public static void sendBlockChange(Player player, Location loc, int type, int data) {
        player.sendBlockChange(loc, type, (byte)data);
    }

    @SuppressWarnings("deprecation")
    public static int getBlockTypeIdAt(World world, int x, int y, int z) {
        return world.getBlockTypeIdAt(x, y, z);
    }

    @SuppressWarnings("deprecation")
    public static int getId(BlockState newState) {
        return newState.getTypeId();
    }

    @SuppressWarnings("deprecation")
    public static short getId(EntityType entity) {
        return entity.getTypeId();
    }

    @SuppressWarnings("deprecation")
    public static void setData(MaterialData data, byte chestData) {
        data.setData(chestData);
    }

    @SuppressWarnings("deprecation")
    public static void setTypeIdAndData(Block block, int type, int data, boolean update) {
        block.setTypeIdAndData(type, (byte)data, update);
    }

    public static ItemStack spawnPlayerHead(String playerName, String itemDisplayName) {
        ItemStack skull = ItemManager.createItemStack(ItemManager.getId(Material.SKULL_ITEM), 1, (short) 3);
        SkullMeta meta = (SkullMeta) skull.getItemMeta();
        meta.setOwner(playerName);
        meta.setDisplayName(itemDisplayName);
        skull.setItemMeta(meta);
        return skull;
    }

    public static boolean removeItemFromPlayer(Player player, Material mat, int amount) {
        ItemStack m = new ItemStack(mat, amount);
        if (player.getInventory().contains(mat)) {
            player.getInventory().removeItem(m);
            player.updateInventory();
            return true;
        }
        return false;
    }

    public static void setChestFacing(Block b, BlockFace dir) {
        b.setType(Material.CHEST);
        BlockState bs = b.getState();
        Chest chest = (Chest) bs.getData();
        chest.setFacingDirection(dir);
        bs.setData(chest);
        bs.update(true);
    }

    /*public static String getDisplayName(ItemStack firstItem) {
        LoreCraftableMaterial craftMat = LoreCraftableMaterial.getCraftMaterial(firstItem);
        if (craftMat == null) {
            String name = firstItem.getType().name().toLowerCase().replace("_", " ");
            switch (firstItem.getType()) {
                case INK_SACK:
                    switch (ItemManager.getData(firstItem)) {
                        case 0:
                            name = "Ink Sack";
                            break;
                        case 1:
                            name = "Rose Red";
                            break;
                        case 2:
                            name = "Cactus Green";
                            break;
                        case 3:
                            name = "Cocoa Beans";
                            break;
                        case 4:
                            name = "Lapis Lazuli";
                            break;
                        case 5:
                            name = "Purple Dye";
                            break;
                        case 6:
                            name = "Cyan Dye";
                            break;
                        case 7:
                            name = "Light Gray Dye";
                            break;
                        case 8:
                            name = "Gray Dye";
                            break;
                        case 9:
                            name ="Pink Dye";
                            break;
                        case 10:
                            name = "Lime Dye";
                            break;
                        case 11:
                            name = "Dandelion Yellow";
                            break;
                        case 12:
                            name = "Light Blue Dye";
                            break;
                        case 13:
                            name = "Magenta Dye";
                            break;
                        case 14:
                            name = "Orange Dye";
                            break;
                        case 15:
                            name = "Bone Meal";
                            break;
                    }
                    break;
                default:
                    name = StringUtils.capitalize(name);
                    break;
            }

			/* vanilla item
            return name;
        } else {
            return craftMat.getName();
        }
    }*/

    public static boolean isWeapon(ItemStack item) {
        switch (ItemManager.getId(item)) {
            case ItemData.WOOD_SWORD:
            case ItemData.STONE_SWORD:
            case ItemData.IRON_SWORD:
            case ItemData.GOLD_SWORD:
            case ItemData.DIAMOND_SWORD:
            case ItemData.BOW:
                return true;
            default:
                return false;
        }
    }

    public static boolean isArmor(ItemStack item) {
        switch (ItemManager.getId(item)) {
            case ItemData.LEATHER_BOOTS:
            case ItemData.LEATHER_CHESTPLATE:
            case ItemData.LEATHER_HELMET:
            case ItemData.LEATHER_LEGGINGS:
            case ItemData.IRON_BOOTS:
            case ItemData.IRON_CHESTPLATE:
            case ItemData.IRON_HELMET:
            case ItemData.IRON_LEGGINGS:
            case ItemData.DIAMOND_BOOTS:
            case ItemData.DIAMOND_CHESTPLATE:
            case ItemData.DIAMOND_HELMET:
            case ItemData.DIAMOND_LEGGINGS:
            case ItemData.CHAIN_BOOTS:
            case ItemData.CHAIN_CHESTPLATE:
            case ItemData.CHAIN_HELMET:
            case ItemData.CHAIN_LEGGINGS:
            case ItemData.GOLD_BOOTS:
            case ItemData.GOLD_CHESTPLATE:
            case ItemData.GOLD_HELMET:
            case ItemData.GOLD_LEGGINGS:
                return true;
            default:
                return false;
        }
    }

    public static byte getRandomWool() {
        Random dice = new Random();
        int number;
        number = dice.nextInt(15);
        switch (number) {
            case 1:
                return (byte) 1;
            case 2:
                return (byte) 2;
            case 3:
                return (byte) 3;
            case 4:
                return (byte) 4;
            case 5:
                return (byte) 5;
            case 6:
                return (byte) 6;
            case 7:
                return (byte) 7;
            case 8:
                return (byte) 8;
            case 9:
                return (byte) 9;
            case 10:
                return (byte) 10;
            case 11:
                return (byte) 11;
            case 12:
                return (byte) 12;
            case 13:
                return (byte) 13;
            case 14:
                return (byte) 14;
            case 15:
                return (byte) 15;
        }
        return (byte) 1;
    }
    
    public static void breakIntoStacks(LinkedList<ItemStack> stacks, ItemStack sourceType, int yield, int maxStackSize) {

        ItemStack stack;
        while (yield > 0) {
            if (yield <= maxStackSize) {
                stack = sourceType.clone();
                stack.setAmount(yield);
            } else {
                stack = sourceType.clone();
                stack.setAmount(maxStackSize);
            }
            stacks.add(stack);
            yield -= maxStackSize;
        }
    }
}
