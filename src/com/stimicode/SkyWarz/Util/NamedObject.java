package com.stimicode.SkyWarz.Util;

/**
 * Created by Derrick on 6/21/2015.
 */
public class NamedObject {
    /* Unique Id of named object. */
    private int id;

    /* Display name of the object. */
    private String name;

    public void setName(String newname)  {
        this.name = sanitize(newname);
    }

    public String getName() {
        return this.name;
    }

    public void setId(int i) {
        this.id = i;
    }

    public int getId() {
        return id;
    }

    public static String sanitize(String name) {
        return name.replace(" ", "_");
    }

    public static boolean isAlpha(String name) {
        return name.matches("[a-zA-Z_]+");
    }

    public static void validateName(String name) throws Exception {

        name = sanitize(name);

        if (name == null) {
            throw new Exception();
        }

        if (!isAlpha(name)) {
            throw new Exception(name+" is invalid, must only contain letters.");
        }

        switch (name.toLowerCase()) {
            case "":
            case "null":
            case "none":
            case "town":
            case "group":
            case "civ":
            case "resident":
            case "cancel":
                throw new Exception(name+" is invalid. It contains restricted words.");
        }

        if (name.length() < 3) {
            throw new Exception(name+" must be longer than 3 characters.");
        }
    }
}
