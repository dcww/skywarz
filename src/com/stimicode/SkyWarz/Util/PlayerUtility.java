package com.stimicode.SkyWarz.Util;

import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * Created by Derrick on 9/10/2015.
 */
public class PlayerUtility {

    public static void setPlayerHealth(Player player, int health) {
        player.setHealth((double) health);
    }

    public static void healPlayer(Player player) {
        player.setHealth(20.0);
    }

    public static void addPlayerHealth(Player player, int health) {
        player.setHealth(player.getHealth() + (double) health);
    }

    public static void setPlayerFood(Player player, int food) {
        player.setFoodLevel(food);
    }

    public static void feedPlayer(Player player) {
        player.setFoodLevel(20);
    }

    public static void addPlayerFood(Player player, int food) {
        player.setFoodLevel(player.getFoodLevel() + food);
    }

    public static void resetPlayerInventory(Player player) {
        player.getInventory().clear();
    }

    public static void setGameMode(Player player, GameMode gameMode) {
        player.setGameMode(gameMode);
    }

    public static void resetInventory(Inventory inventory) {
        inventory.clear();
    }

    public static void removePotionEffect(Player player, PotionEffectType effect) {
        player.removePotionEffect(effect);
    }

    public static void resetPlayerPotionEffects(Player player) {
        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
    }

    public static void completelyResetPlayer(Player player) {
        healPlayer(player);
        feedPlayer(player);
        resetPlayerInventory(player);
        resetPlayerPotionEffects(player);
        setGameMode(player, GameMode.SURVIVAL);
    }

    public static void feedAndHealPlayer(Player player) {
        healPlayer(player);
        feedPlayer(player);
    }

    public static void resetPlayerChat(Player player) {
        for (int x = 0; x < 25; x++) {
            MessageManager.send(player, "");
        }
    }
}
