package com.stimicode.SkyWarz.Tasks;

import com.stimicode.SkyWarz.Managers.GameStateManager;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 8/4/2015.
 */
public class ServerEnd implements Runnable {

    private int x = 10;

    @Override
    public void run() {
        if (Manager.gameStateManager.isState(GameStateManager.PostGame)) {
            if (x == 0) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.kickPlayer("Server Reboot!");
                }
                Bukkit.shutdown();
                //TODO Need to add Map Reset Code.
            } else {
                MessageManager.sendAll(C.B_Red+"Server Reboot in "+x+" seconds!");
                x--;
            }
        } else {
            x = 10;
        }
    }
}
