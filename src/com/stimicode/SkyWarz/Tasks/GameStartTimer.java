package com.stimicode.SkyWarz.Tasks;

import com.stimicode.SkyWarz.APIs.TitleAPI.Title;
import com.stimicode.SkyWarz.Managers.GameStateManager;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 8/6/2015.
 */
public class GameStartTimer implements Runnable {

    private int x = 10;

    @Override
    public void run() {
        if (Manager.gameStateManager.isState(GameStateManager.PreGame)) {
            if (x == 0) {
                Manager.gameStateManager.setState(GameStateManager.InGame);
                for (Player player : Bukkit.getOnlinePlayers()) {
                    Title.sendTitle(player, 1, 50, 10, C.B_Green + "Start!", C.B_Green + "Have Fun!");
                }
            } else {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    Title.sendTitle(player, 1, 22, 0, C.B_Gold+"Game Starts in", C.B_Aqua+x+" seconds!");
                }
                x--;
            }
        }
    }
}
