package com.stimicode.SkyWarz.Tasks;

import com.stimicode.SkyWarz.APIs.TitleAPI.Title;
import com.stimicode.SkyWarz.Game.Game;
import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Managers.GameStateManager;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Derrick on 8/4/2015.
 */
public class GameVictoryTimer implements Runnable {

    @Override
    public void run() {
        if (Manager.gameStateManager.isState(GameStateManager.InGame)) {
            if (Game.gamePlayers.size() == 1) {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    Title.sendTitle(player, 1, 50, 10, C.B_Green + "Congratulations, " + Bukkit.getPlayer(UUID.fromString(Game.gamePlayers.get(0))).getDisplayName() + "!", C.B_Green + "Has won the game!");
                    Manager.gameStateManager.setState(GameStateManager.PostGame);
                }
            }
        }
    }
}