package com.stimicode.SkyWarz.Tasks;

import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Managers.CoreAccountManager;
import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by Derrick on 6/21/2015.
 */
public class PlayerKickBan implements Runnable {

    UUID uuid;
    boolean kick;
    boolean ban;
    String reason;

    public PlayerKickBan(UUID uuid, boolean kick, boolean ban, String reason) {
        this.uuid = uuid;
        this.kick = kick;
        this.ban = ban;
        this.reason = reason;
    }

    @Override
    public void run() {
        Player player = null;
        try {
            player = CoreAccountManager.getPlayer(uuid);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (player == null) {
            Log.error("Couldn't find player with UUID:" + uuid);
            return;
        }

        if (kick) {
            player.kickPlayer(reason);
        }
    }
}