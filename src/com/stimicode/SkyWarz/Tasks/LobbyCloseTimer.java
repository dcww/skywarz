package com.stimicode.SkyWarz.Tasks;

import com.stimicode.SkyWarz.APIs.TitleAPI.Title;
import com.stimicode.SkyWarz.Game.Game;
import com.stimicode.SkyWarz.Managers.GameStateManager;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 7/3/2015.
 */
public class LobbyCloseTimer implements Runnable {

    private int x = 10;

    @Override
    public void run() {
        if (Manager.gameStateManager.isState(GameStateManager.Lobby)) {
            if (Bukkit.getOnlinePlayers().size() >= Game.minimumPCount || Game.gameStartOverride) {
                if (x == 0) {
                    Game.chooseMap();
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        Game.addGamePlayer(player);
                        Title.sendTitle(player, 1, 50, 10, C.B_Red + "Lobby Closed!", "");
                    }
                    Manager.gameStateManager.setState(GameStateManager.PreGame);
                } else {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        Title.sendTitle(player, 1, 22, 0, C.B_Gold+"Lobby Closes in", C.B_Aqua+x+" seconds!");
                    }
                    x--;
                }
            } else {
                x = 10;
            }
        }
    }
}
