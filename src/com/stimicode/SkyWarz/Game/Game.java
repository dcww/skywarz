package com.stimicode.SkyWarz.Game;

import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.Rank;
import com.stimicode.SkyWarz.Object.MultiWorld;
import com.stimicode.SkyWarz.Object.SpawnLocation;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Created by Derrick on 7/27/2015.
 */
public class Game {

    public static ArrayList<String> gamePlayers = new ArrayList<>();                        //Players in game.

    public static int playerCount;                                                          //The amount of players per game
    public static int minimumPCount;

    public static boolean ignoreStaff;                                                      //Whether or not staff are ignored
    public static Rank ignoredStaff;                                                        //The Ignored Staff Rank

    public static boolean gameStartOverride = false;                                        //A Variable that is set with a command which overrides the starting of the game!

    public static MultiWorld activeMap;                                                     //The active map that was chosen at game start.

    public static int mapNumber;

    public static void init() {
        try {
            playerCount = Manager.settingsManager.getInteger("player_count");
            ignoreStaff = Manager.settingsManager.getBoolean("ignore_staff");
            ignoredStaff = Rank.getRank(Manager.settingsManager.getStringBase("staff_rank"));
            minimumPCount = Manager.settingsManager.getInteger("minimum_count");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Manager.worldManager.setupMaps();
    }

    public static void addGamePlayer(Player player) {
        gamePlayers.add(player.getUniqueId().toString());
        for (SpawnLocation loc : activeMap.spawnLocations.values()) {
            player.teleport(loc.getLocation());
            if (!(loc.isOccupied())) {
                loc.setOccupied(true);
                loc.setAccount(Manager.accountManager.getAccount(player));
                player.teleport(loc.getLocation());
                return;
            }
        }
    }

    public static void removeGamePlayer(Player player) {
        gamePlayers.remove(player.getUniqueId().toString());
    }

    public static boolean hasGamePlayer(Player player) {
        return gamePlayers.contains(player.getUniqueId().toString());
    }

    public static void chooseMap() {
        ArrayList<MultiWorld> temp = new ArrayList<>();
        Random random = new Random();
        int chance = random.nextInt(mapNumber);
        temp.addAll(Manager.worldManager.worlds.values().stream().filter(multiWorld -> multiWorld.isGameWorld()).collect(Collectors.toList()));
        activeMap = temp.get(chance);
        MessageManager.sendAll(C.B_Gold + "Map Chosen: " + activeMap.getWorld().getName());
    }

    public static void setSpawn(String number, SpawnLocation loc) throws Exception {
        MultiWorld multiWorld = Manager.worldManager.worlds.get(loc.getLocation().getWorld().getName());
        if (Integer.valueOf(number) < (playerCount + 1)) {
            if (!(multiWorld.spawnLocations.containsKey(number))) {
                multiWorld.spawnLocations.put(number, loc);
            } else {
                multiWorld.spawnLocations.replace(number, loc);
            }
            Manager.settingsManager.setLocation(multiWorld.getName(), number, loc);
        } else {
            throw new Exception("Can only edit spawns 1 through "+playerCount+"!");
        }
    }
}
