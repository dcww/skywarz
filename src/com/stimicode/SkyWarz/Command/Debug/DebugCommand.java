package com.stimicode.SkyWarz.Command.Debug;

import com.stimicode.SkyWarz.APIs.TitleAPI.Title;
import com.stimicode.SkyWarz.Command.CommandBase;
import com.stimicode.SkyWarz.Game.Game;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.Rank;
import com.stimicode.SkyWarz.Object.LootItem;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 6/25/2015.
 */
public class DebugCommand extends CommandBase {
    @Override
    public void init() {
        command = "/debug";
        displayName = "Debug";

        commands.put("killall", "Removes all entities in players world.");
        commands.put("clearinventory", "Removes all items in players inventory");
        commands.put("ignorestaff", "Returns boolean to ignorestaff");
        commands.put("ignoredstaff", "Returns what rank to ignore");
        commands.put("sendtitle", "");
        commands.put("teleport", "");
        commands.put("saveloot", "");
    }

    public void saveloot_cmd() throws Exception {
        for (LootItem test : Manager.settingsManager.lootItemMap.values()) {

        }
    }

    public void teleport_cmd() throws Exception {
        getPlayer().teleport(new Location(getPlayer().getWorld(), 245+0.5, 54, 1348+0.5));
    }

    public void sendtitle_cmd() throws Exception {
        Title.sendTitle(getPlayer(), 20, 100, 20, C.B_Aqua+"PartyChat", C.B_Gold+"Chat With Your Friends!");
    }

    public void ignorestaff_cmd() throws Exception {
        MessageManager.send(getPlayer(), "" + Game.ignoreStaff);
    }

    public void ignoredstaff_cmd() throws Exception {
        MessageManager.send(getPlayer(), Rank.getRankName(Game.ignoredStaff));
    }

    public void killall_cmd() throws Exception {
        int x = 0;
        for (Entity entity :getPlayer().getWorld().getEntities()) {
            if (!(entity instanceof Player) && !(entity instanceof Item)) {
                entity.remove();
                x++;
            }
        }
        MessageManager.send(getAccount(), "Removed: " + x + " entities.");
    }

    public void clearinventory_cmd() throws Exception {
        getPlayer().getInventory().clear();
        MessageManager.send(getAccount(), "Cleared inventory.");
    }

    @Override
    public void doDefaultAction() throws Exception {
        showHelp();
    }

    @Override
    public void showHelp() {
        showBasicHelp();
    }

    @Override
    public void permissionCheck() throws Exception {
        if (sender instanceof Player) {
            if (getAccount().getRank().has(getPlayer(), Rank.Administrator, false)) {
                return;
            }
        }

        if (!(sender.isOp())) {
            throw new Exception("Only admins can use this command.");
        }
    }
}