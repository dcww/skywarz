package com.stimicode.SkyWarz.Command;

import com.stimicode.SkyWarz.Managers.CommandManager;
import com.stimicode.SkyWarz.Managers.Rank;
import org.bukkit.entity.Player;

import java.util.Collection;

/**
 * Created by Derrick on 6/21/2015.
 */
public abstract class CommandInterface {
    public abstract void setCommandCenter(CommandManager paramCommandCenter);

    public abstract void executeCommand(Player paramPlayer, String[] paramArrayOfString);

    public abstract Collection<String> Aliases();

    public abstract void setAlias(String paramString);

    public abstract Rank getRequiredRank();
}
