package com.stimicode.SkyWarz.Command.Admin;

import com.stimicode.SkyWarz.Command.CommandBase;
import com.stimicode.SkyWarz.Game.Game;
import com.stimicode.SkyWarz.Managers.GameStateManager;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Object.SpawnLocation;

/**
 * Created by Derrick on 7/28/2015.
 */
public class AdminGameCommand extends CommandBase {

    @Override
    public void init() {
        command = "/admin game";
        displayName = "Admin Game";

        commands.put("setspawn", "[Number] (Up to " + Game.playerCount + ") - Sets the location of the given numbered spawn.");
        commands.put("state", "[State] - Sets the games state to the given state. (PreGame, InGame, PostGame)");
        commands.put("startoverride", "Toggles the Game Start Override");
        commands.put("ignorestaff", "");
    }

    public void ignorestaff_cmd() throws Exception {
        Game.ignoreStaff = !(Game.ignoreStaff);
    }

    public void startoverride_cmd() throws Exception {
        Game.gameStartOverride = !(Game.gameStartOverride);
    }

    public void state_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a game state! (PreGame, InGame, PostGame)");
        }

        if (args[1].equalsIgnoreCase("pregame") || args[1].equalsIgnoreCase("ingame") || args[1].equalsIgnoreCase("postgame")) {
            Manager.gameStateManager.setState(GameStateManager.getGameState(args[1]));
        } else {
            throw new Exception("Please enter a valid game state! (PreGame, InGame, PostGame)");
        }
    }

    public void setspawn_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a spawn number. (Between 1-"+ Game.playerCount +")");
        }

        if (!(Manager.worldManager.worlds.get(getPlayer().getWorld().getName()).isGameWorld())) {
            throw new Exception("This world is not a game world. You can only set multiple spawns on game worlds.");
        }
        Game.setSpawn(args[1], new SpawnLocation(getPlayer().getLocation(), false));
    }

    @Override
    public void doDefaultAction() throws Exception {
        showHelp();
    }

    @Override
    public void showHelp() {
        showBasicHelp();
    }

    @Override
    public void permissionCheck() throws Exception {

    }
}
