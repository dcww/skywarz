package com.stimicode.SkyWarz.Command.Admin;

import com.stimicode.SkyWarz.Command.CommandBase;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.Rank;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;

/**
 * Created by Derrick on 6/23/2015.
 */
public class AdminCommand extends CommandBase {
    @Override
    public void init() {
        command = "/admin";
        displayName = "Admin";

        commands.put("test", "This is just a test command to test whether or not the system works.");
        commands.put("rank", "Brings up the ranking command menu.");
        commands.put("game", "Brings up the game command menu.");
        commands.put("token", "Brings up the token command menu.");
        commands.put("world", "Brings up the world command menu.");
        commands.put("menu", "Opens menu.");
        commands.put("checkchests", "fd");
    }

    public void menu_cmd() throws Exception {

    }

    public void world_cmd() throws Exception {
        AdminWorldCommand cmd = new AdminWorldCommand();
        cmd.onCommand(sender, null, "world", this.stripArgs(args, 1));
    }

    public void game_cmd() throws Exception {
        AdminGameCommand cmd = new AdminGameCommand();
        cmd.onCommand(sender, null, "game", this.stripArgs(args, 1));
    }

    public void rank_cmd() throws Exception {
        AdminRankCommand cmd = new AdminRankCommand();
        cmd.onCommand(sender, null, "rank", this.stripArgs(args, 1));
    }

    public void token_cmd() throws Exception {
        AdminTokenCommand cmd = new AdminTokenCommand();
        cmd.onCommand(sender, null, "token", this.stripArgs(args, 1));
    }

    public void checkchests_cmd() throws Exception {
        for (Chunk chunk : getPlayer().getWorld().getLoadedChunks()) {
            for (BlockState block : chunk.getTileEntities()) {
                if (block.getType().equals(Material.CHEST)) {
                    Block bloc = getPlayer().getWorld().getBlockAt(block.getX(), block.getY(), block.getZ());
                    Chest chest = (Chest) bloc.getState();
                    if (chest.getInventory().contains(Material.STONE)) {
                        MessageManager.send(getPlayer(), bloc.getLocation().toString());
                    }
                }
            }
        }
    }

    public void chestchecker_cmd() throws Exception {

    }

    @Override
    public void doDefaultAction() throws Exception {
        showHelp();
    }

    @Override
    public void showHelp() {
        showBasicHelp();
    }

    @Override
    public void permissionCheck() throws Exception {
        if (sender instanceof Player) {
            if (getAccount().getRank().has(getPlayer(), Rank.Administrator, false)) {
                return;
            }
        }

        if (!sender.hasPermission("test")) {
            throw new Exception("TESTTESTSTEt");
        }

        if (!(sender.isOp())) {
            throw new Exception("Only admins can use this command.");
        }
    }
}
