package com.stimicode.SkyWarz.Command.Admin;


import com.stimicode.SkyWarz.Account.CoreAccount;
import com.stimicode.SkyWarz.Command.CommandBase;
import com.stimicode.SkyWarz.Managers.CoreAccountManager;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.Rank;

/**
 * Created by Derrick on 6/24/2015.
 */
public class AdminRankCommand extends CommandBase {

    @Override
    public void init() {
        command = "/admin rank";
        displayName = "Admin Ranking";

        commands.put("set", "[Name] [Rank]");
        commands.put("reset", "[Name]");
        commands.put("ranks", "Lists all of the ranks");
    }

    public void set_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a player name.");
        }

        CoreAccount account = getNamedAccount(1);
        if (account == null) {
            throw new Exception("No account with given name.");
        }

        if (args.length < 3) {
            throw new Exception("Please enter a rank.");
        }

        Rank rank = Rank.getRank(args[2]);
        if (rank == null) {
            throw new Exception("Given rank does not exist.");
        }
        account.setRank(rank);
        MessageManager.send(getAccount(), "" + account.getName() + "'s rank has been set to " + Rank.getRankName(rank));
        MessageManager.send(account, "Your rank has been changed to " + Rank.getRankName(rank) + " by " + getAccount().getName() + ".");
        account.save();
    }

    public void reset_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a player name.");
        }

        CoreAccount account = getNamedAccount(1);
        if (account == null) {
            throw new Exception("No account with given name.");
        }

        Rank rank = Rank.getRank("default");
        account.setRank(rank);
        MessageManager.send(getAccount(), "" + account.getName() + "'s rank has been reset to " + Rank.getRankName(rank));
        MessageManager.send(account, "Your rank has been reset to "+ Rank.getRankName(rank)+" by "+getAccount().getName()+".");
        account.save();
    }

    public void ranks_cmd() throws Exception {
        String message = "";
        for (String string : Rank.ranks) {
            message += string;
        }
        MessageManager.send(getAccount(), message);
    }

    @Override
    public void doDefaultAction() throws Exception {
        showHelp();
    }

    @Override
    public void showHelp() {
        showBasicHelp();
    }

    @Override
    public void permissionCheck() throws Exception {

    }
}
