package com.stimicode.SkyWarz.Command.Admin;

import com.avaje.ebeaninternal.server.cluster.mcast.Message;
import com.stimicode.SkyWarz.Command.CommandBase;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Object.MultiWorld;
import com.stimicode.SkyWarz.Object.SpawnLocation;
import com.stimicode.SkyWarz.Util.C;
import com.stimicode.SkyWarz.Util.F;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.libs.org.ibex.nestedvm.Runtime;

/**
 * Created by Derrick on 8/6/2015.
 */
public class AdminWorldCommand extends CommandBase {

    @Override
    public void init() {
        command = "/admin world";
        displayName = "Admin World";

        commands.put("teleport", "[World Name] - Teleports to the given world.");
        commands.put("list", "Lists all of the currently loaded worlds");
        commands.put("save", "[World Name] - Saves the given world.");
        commands.put("remove", "[World Name] - Removes the given world.");
        commands.put("reset", "[World Name] - Resets the given world from it's backup.");
        commands.put("create", "[World Name] [Template Name] - Creates a new world from the template chosen. Type 'new' in template if you would like a world generated.");
        commands.put("listspawns", "[World Name] - Lists the spawn locations of the given world.");
        commands.put("setspawn", "[World Name] - Sets the worlds spawn point for the given world.");
    }

    public void setspawn_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a world name!");
        }

        World world = Bukkit.getWorld(args[1]);

        if (world == null) {
            throw new Exception("Please enter a valid world name!");
        }

        MultiWorld multiWorld = Manager.worldManager.worlds.get(args[1]);

        if (multiWorld == null) {
            throw new Exception("No MultiWorld for the given world.");
        }

        Location loc = getPlayer().getLocation();
        world.setSpawnLocation(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        MessageManager.send(getPlayer(), "Set worlds spawn location!");
    }

    public void remove_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a world name!");
        }

        World world = Bukkit.getWorld(args[1]);

        if (world == null) {
            throw new Exception("Please enter a valid world name!");
        }

        MultiWorld multiWorld = Manager.worldManager.worlds.get(args[1]);

        if (multiWorld == null) {
            throw new Exception("No MultiWorld for the given world... Not good.");
        }

        Manager.worldManager.removeWorld(args[1]);
    }

    public void reset_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a world name!");
        }

        World world = Bukkit.getWorld(args[1]);

        if (world == null) {
            throw new Exception("Please enter a valid world name!");
        }

        MultiWorld multiWorld = Manager.worldManager.worlds.get(args[1]);

        if (multiWorld == null) {
            throw new Exception("No MultiWorld for the given world... Not good.");
        }

        MessageManager.send(getPlayer(), "Preparing to reset world!");
        Manager.worldManager.resetWorld(args[1]);
        MessageManager.send(getPlayer(), "Successfully reset world!");
    }

    public void create_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a world name!");
        }

        if (args.length < 3) {
            throw new Exception("Please enter a template world name! (or type 'new')");
        }

        World world = Bukkit.getWorld(args[1]);

        if (world != null) {
            throw new Exception("World already exists with the given name!");
        }

        if (Manager.worldManager.worlds.containsKey(args[1])) {
            throw new Exception("MultiWorld already exists with the given name!");
        }

        Manager.worldManager.createNewWorld(args[1], args[2]);
        MessageManager.send(getPlayer(), "Successfully create world!");
    }

    public void save_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a world name!");
        }

        World world = Bukkit.getWorld(args[1]);

        if (world == null) {
            throw new Exception("Please enter a valid world name!");
        }

        MultiWorld multiWorld = Manager.worldManager.worlds.get(args[1]);

        if (multiWorld == null) {
            throw new Exception("No MultiWorld for the given world... Not good.");
        }

        MessageManager.send(getPlayer(), "Preparing to save world to template!");
        Manager.worldManager.saveWorldToTemplate(args[1]);
        MessageManager.send(getPlayer(), "Successfully saved world to template!");
    }

    public void listspawns_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a world name!");
        }

        World world = Bukkit.getWorld(args[1]);

        if (world == null) {
            throw new Exception("Please enter a valid world name!");
        }

        MultiWorld multiWorld = Manager.worldManager.worlds.get(args[1]);

        if (multiWorld == null) {
            throw new Exception("No MultiWorld for the given world... Not good.");
        }

        if (multiWorld.getSpawnLocations().isEmpty()) {
            throw new Exception("No listed spawns for this MultiWorld.");
        }

        for (SpawnLocation loc : multiWorld.getSpawnLocations().values()) {
            MessageManager.send(getPlayer(), F.locationFormatter(loc.getLocation()));
            MessageManager.sendHoloMessage(loc.getLocation().add(0,3,0), 10, "Spawn Point: "+loc.getName());
        }
    }

    public void list_cmd() throws Exception {
        MessageManager.send(getPlayer(), C.B_Aqua+MessageManager.buildTitle("Worlds"));
        for (World world : Bukkit.getServer().getWorlds()) {
            if (Manager.worldManager.worlds.containsKey(world.getName())) {
                MessageManager.send(getPlayer(), world.getName() + " [Custom World]");
            } else {
                MessageManager.send(getPlayer(), world.getName() + " [Vanilla World]");
            }
        }
    }

    public void teleport_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a world name!");
        }
        World world = Bukkit.getWorld(args[1]);

        if (world == null) {
            throw new Exception("Please enter a valid world name!");
        }

        getPlayer().teleport(world.getSpawnLocation());
    }

    @Override
    public void doDefaultAction() throws Exception {
        showHelp();
    }

    @Override
    public void showHelp() {
        showBasicHelp();
    }

    @Override
    public void permissionCheck() throws Exception {

    }
}
