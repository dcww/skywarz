package com.stimicode.SkyWarz.Command.Admin;

import com.stimicode.SkyWarz.Account.CoreAccount;
import com.stimicode.SkyWarz.Command.CommandBase;
import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Managers.MessageManager;

/**
 * Created by Derrick on 6/30/2015.
 */
public class AdminTokenCommand extends CommandBase {
    @Override
    public void init() {
        command = "/admin token";
        displayName = "Admin Economy Commands";

        commands.put("set", "[Name] [Token]");
        commands.put("add", "[Name] [Token]");
        commands.put("remove", "[Name] [Token]");
        commands.put("reset", "[Name]");
        commands.put("check", "Lists all of the ranks");
    }

    public void check_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a player name.");
        }

        CoreAccount account = getNamedAccount(1);
        if (account == null) {
            throw new Exception("No account with given name.");
        }

        MessageManager.send(getAccount(), "" + account.getName() + " has " + account.getTokens() + " Tokens.");
    }

    public void reset_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a player name.");
        }

        CoreAccount account = getNamedAccount(1);
        if (account == null) {
            throw new Exception("No account with given name.");
        }

        account.setTokens(0);
        MessageManager.send(getAccount(), "" + account.getName() + "'s Tokens have been set to " + args[2]);
        MessageManager.send(account, "Your Tokens have been set to " + args[2] + " by " + getAccount().getName() + ".");
        account.save();
    }

    public void set_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a player name.");
        }

        CoreAccount account = getNamedAccount(1);
        if (account == null) {
            throw new Exception("No account with given name.");
        }

        if (args.length < 3) {
            throw new Exception("Please enter a Token amount.");
        }
        account.setTokens(Integer.valueOf(args[2]));
        MessageManager.send(getAccount(), "" + account.getName() + "'s Tokens have been set to " + args[3]);
        MessageManager.send(account, "Your Tokens have been set to " + args[3] + " by " + getAccount().getName() + ".");
        account.save();
    }

    public void add_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a player name.");
        }

        CoreAccount account = getNamedAccount(1);
        if (account == null) {
            throw new Exception("No account with given name.");
        }

        if (args.length < 3) {
            throw new Exception("Please enter a Token amount.");
        }
        Log.debug("Args[3]: " + args[2]);
        account.addTokens(Integer.valueOf(args[2]));
        MessageManager.send(getAccount(), "" + account.getName() + "'s Tokens have been set to "+args[2]);
        MessageManager.send(account, "Your Tokens have been set to "+args[2]+" by "+getAccount().getName()+".");
        account.save();
    }

    public void remove_cmd() throws Exception {
        if (args.length < 2) {
            throw new Exception("Please enter a player name.");
        }

        CoreAccount account = getNamedAccount(1);
        if (account == null) {
            throw new Exception("No account with given name.");
        }

        if (args.length < 3) {
            throw new Exception("Please enter a Token amount.");
        }
        account.removeTokens(Integer.parseInt(args[2]));
        MessageManager.send(getAccount(), "" + account.getName() + "'s Tokens have been set to "+args[2]);
        MessageManager.send(account, "Your Tokens have been set to "+args[2]+" by "+getAccount().getName()+".");
        account.save();
    }

    @Override
    public void doDefaultAction() throws Exception {
        showHelp();
    }

    @Override
    public void showHelp() {
        showBasicHelp();
    }

    @Override
    public void permissionCheck() throws Exception {

    }
}
