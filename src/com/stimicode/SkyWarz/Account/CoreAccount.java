package com.stimicode.SkyWarz.Account;

import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Managers.CoreAccountManager;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.Rank;
import com.stimicode.SkyWarz.SQL.SQL;
import com.stimicode.SkyWarz.SQL.SQLObject;
import com.stimicode.SkyWarz.SQL.SQLUpdate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Derrick on 6/14/2015.
 */
public class CoreAccount extends SQLObject {
    //private int id;
    private String l_currentName;
    private String l_currentIP;
    private String l_uuid;
    private Rank l_rank;
    private Player l_player;
    private ArrayList<String> l_nameHistory = new ArrayList<>();
    private ArrayList<String> l_ipHistory = new ArrayList<>();
    private int tokens;

    private long registered;
    private long lastOnline;

    private ArrayList<Package> packages = new ArrayList<>();

    //private HashMap<String, String> nameHistory = new HashMap<String, String>();
    //private ArrayList<Package> packages = new ArrayList<Package>();
    //private Rank rank;

    public CoreAccount(String name, UUID uid) {
        this.l_currentName = name;
        this.l_uuid = uid.toString();
        updateNameHistory(this.l_currentName);
    }

    public CoreAccount(Player player) {
        this.l_player = player;
        this.l_currentName = player.getName();
        this.l_uuid = player.getUniqueId().toString();
        updateNameHistory(this.l_currentName);
    }

    public CoreAccount(String name) {
        this.l_currentName = name;
        updateNameHistory(this.l_currentName);
    }

    public CoreAccount(ResultSet rs) throws Exception {
        this.load(rs);
    }

    public void updateNameHistory(String checkable) {
        if (!this.l_nameHistory.contains(checkable)) {
            this.l_nameHistory.add(checkable);
        }
    }

    public void updateIPHistory(String checkable) {
        if (!this.l_ipHistory.contains(checkable)) {
            this.l_ipHistory.add(checkable);
        }
    }

    private String getNameHistorySaveString() {
        String out = "";
        for (String name : l_nameHistory) {
            out += name+",";
        }
        return out;
    }

    private void loadNameHistoryFromSaveString(String string) {
        String[] split = string.split(",");

        Collections.addAll(l_nameHistory, split);
    }

    private String getIPHistorySaveString() {
        String out = "";
        for (String name : l_ipHistory) {
            out += name+",";
        }
        return out;
    }

    private void loadIPHistoryFromSaveString(String string) {
        String[] split = string.split(",");

        Collections.addAll(l_ipHistory, split);
    }

    public int getTokens() { return this.tokens; }

    public void setTokens(int tokens) { this.tokens = tokens; }

    public void addTokens(int tokens) { this.tokens += tokens; }

    public void removeToken(int tokens) { this.tokens -= tokens; }

    public boolean removeTokens(int tokens) {
        this.tokens -= tokens;
        return (this.tokens >= 0);
    }

    public Player getPlayer() { return this.l_player; }

    public void setPlayer(Player player) { this.l_player = player; }

    public String getName() { return this.l_currentName; }

    public void setName(String newName) { this.l_currentName = newName; }

    public String getIP() { return this.l_currentIP; }

    public void setIP(String string) {
        this.l_currentIP = string;
        this.updateIPHistory(string);
    }

    //public int getAccountID() { return this.id; }

    //public void setAccountID(int accountID) { this.id = accountID; }

    public UUID getUuid() { return UUID.fromString(this.l_uuid); }

    public String getUuidString() { return this.l_uuid; }

    public void setUuid(String uid) { this.l_uuid = uid; }

    public void setRank(Rank rank) { this.l_rank = rank; }

    public Rank getRank() { return this.l_rank; }

    public boolean hasRank(Rank rank, boolean inform) { return this.l_rank.has(this, rank, inform); }

    public void handleNameChange(String newName) {
        CoreAccount temp = Manager.accountManager.getAccount(newName);
        if (temp != null) {
            temp.setName(temp.getUuidString());
            temp.save();
        }
    }

    public long getRegistered() {
        return registered;
    }

    public void setRegistered(long registered) {
        this.registered = registered;
    }

    public long getLastOnline() {
        return lastOnline;
    }

    public void setLastOnline(long lastOnline) {
        this.lastOnline = lastOnline;
    }

    public static final String TABLE_NAME = "CoreAccounts";
    public static void init() throws SQLException {
        if (!SQL.hasTable(TABLE_NAME)) {
            String table_create = "CREATE TABLE " + SQL.tb_prefix + TABLE_NAME+" (" +
                    "`id` int(11) unsigned NOT NULL auto_increment," +
                    "`Current_Name` VARCHAR(64) NOT NULL," +
                    "`UUID` VARCHAR(256) NOT NULL,"+
                    "`Last_IP` mediumtext NOT NULL,"+
                    "`Name_History` mediumtext NOT NULL," +
                    "`IP_History` mediumtext NOT NULL," +
                    "`Last_Online` BIGINT NOT NULL," +
                    "`Registered` BIGINT NOT NULL," +
                    "`Rank` mediumtext NOT NULL," +
                    "`Tokens` BIGINT NOT NULL," +
                    //"UNIQUE KEY (`Current_Name`), " +
                    "PRIMARY KEY (`id`)" + ")";

            SQL.makeTable(table_create);
            Log.info("Created " + TABLE_NAME + " table");
        } else {
            Log.info(TABLE_NAME+" table OK!");

            if (!SQL.hasColumn(TABLE_NAME, "UUID")) {
                Log.info("\tCouldn't find `UUID` for Account.");
                SQL.addColumn(TABLE_NAME, "`UUID` VARCHAR(256) NOT NULL DEFUALT 'UNKOWN'");
            }

            if (!SQL.hasColumn(TABLE_NAME, "Current_Name")) {
                Log.info("\tCouldn't find `Current_Name` for Account.");
                SQL.addColumn(TABLE_NAME, "`Current_Name` VARCHAR(64) DEFAULT NULL");
            }

            if (!SQL.hasColumn(TABLE_NAME, "Last_IP")) {
                Log.info("\tCouldn't find `Last_IP` for Account.");
                SQL.addColumn(TABLE_NAME, "`Last_IP` mediumtext DEFAULT NULL");
            }

            if (!SQL.hasColumn(TABLE_NAME, "Name_History")) {
                Log.info("\tCouldn't find `Name_History` for Account.");
                SQL.addColumn(TABLE_NAME, "`Name_History` mediumtext DEFAULT NULL");
            }

            if (!SQL.hasColumn(TABLE_NAME, "IP_History")) {
                Log.info("\tCouldn't find `IP_History` for Account.");
                SQL.addColumn(TABLE_NAME, "`IP_History` mediumtext DEFAULT NULL");
            }

            if (!SQL.hasColumn(TABLE_NAME, "Registered")) {
                Log.info("\tCouldn't find `Registered` for Account.");
                SQL.addColumn(TABLE_NAME, "`Registered` BIGINT DEFAULT NULL");
            }

            if (!SQL.hasColumn(TABLE_NAME, "Last_Online")) {
                Log.info("\tCouldn't find `Last_Online` for Account.");
                SQL.addColumn(TABLE_NAME, "`Last_Online` BIGINT DEFAULT NULL");
            }

            if (!SQL.hasColumn(TABLE_NAME, "Rank")) {
                Log.info("\tCouldn't find `Rank` for Account.");
                SQL.addColumn(TABLE_NAME, "`Rank` mediumtext DEFAULT NULL");
            }

            if (!SQL.hasColumn(TABLE_NAME, "Tokens")) {
                Log.info("\tCouldn't find `Tokens` for Account.");
                SQL.addColumn(TABLE_NAME, "`Tokens` BIGINT DEFAULT '0'");
            }
        }
    }

    @Override
    public void load(ResultSet rs) throws Exception {
        this.setId(rs.getInt("id"));
        this.l_currentName = rs.getString("Current_Name");
        this.l_player = Bukkit.getPlayer(l_currentName);
        this.l_uuid = rs.getString("UUID");
        this.l_currentIP = rs.getString("Last_IP");
        this.loadNameHistoryFromSaveString(rs.getString("Name_History"));
        this.loadIPHistoryFromSaveString(rs.getString("IP_History"));

        this.registered = rs.getLong("Registered");
        this.lastOnline = rs.getLong("Last_Online");
        this.l_rank = Rank.getRank(rs.getString("Rank"));
        this.tokens = rs.getInt("Tokens");
    }

    @Override
    public void save() {
        SQLUpdate.add(this);
    }

    @Override
    public void saveNow() throws SQLException {
        HashMap<String, Object> hashmap = new HashMap<>();

        hashmap.put("Current_Name", this.l_currentName);
        hashmap.put("UUID", this.l_uuid);
        hashmap.put("Last_IP", this.l_currentIP);
        hashmap.put("Name_History", this.getNameHistorySaveString());
        hashmap.put("IP_History", this.getIPHistorySaveString());
        hashmap.put("Registered", this.registered);
        hashmap.put("Last_Online", this.lastOnline);
        hashmap.put("Rank", Rank.getRankName(l_rank));
        hashmap.put("Tokens", this.tokens);
        SQL.updateNamedObject(this, hashmap, TABLE_NAME);
    }

    @Override
    public void delete() throws SQLException {

    }
}
