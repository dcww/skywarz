package com.stimicode.SkyWarz.Configurations;

import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Object.LootItem;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;
import java.util.Map;

/**
 * Created by Derrick on 7/28/2015.
 */
public class LootConfig {

    public static void loadConfig(FileConfiguration cfg, Map<String, LootItem> lootItems){
        lootItems.clear();
        List<Map<?, ?>> lootItemTemp = cfg.getMapList("loot");
        System.out.println(lootItemTemp.toString());
        for (Map<?, ?> temp : lootItemTemp) {
            System.out.println("Material from Config: "+temp.get("material"));
            LootItem lootItem = new LootItem(
                    Material.getMaterial((String) temp.get("material")),
                    (Integer) temp.get("byte"),
                    (Integer) temp.get("chance"),
                    (Integer) temp.get("amount")
            );
            lootItems.put((String) temp.get("material"), lootItem);
        }
        Log.info("Loaded " + lootItemTemp.size() + " Loot Items.");
    }
}
