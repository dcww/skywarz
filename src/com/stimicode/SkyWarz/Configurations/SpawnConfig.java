package com.stimicode.SkyWarz.Configurations;

import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Object.SpawnLocation;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

import java.util.List;
import java.util.Map;

/**
 * Created by Derrick on 7/28/2015.
 */
public class SpawnConfig {

    public static void loadConfig(FileConfiguration cfg, Map<Integer, SpawnLocation> spawnLocations){
        spawnLocations.clear();
        List<Map<?, ?>> spawnLocationTemp = cfg.getMapList("spawns");
        System.out.println(spawnLocationTemp.toString());
        String tempWorld = null;
        int x = 1;
        for (Map<?, ?> temp : spawnLocationTemp) {
            double temp1 = (Double) temp.get("pitch");
            double temp2 = (Double) temp.get("yaw");

            SpawnLocation spawnLocation = new SpawnLocation();
            spawnLocation.setLocation(new Location(
                    Bukkit.getWorld((String) temp.get("world")),
                    (Double) temp.get("x"),
                    (Double) temp.get("y"),
                    (Double) temp.get("z"),
                    (float) temp2,
                    (float) temp1
            ));

            spawnLocation.setOccupied(false);
            spawnLocation.setName((String) temp.get("id"));
            tempWorld = spawnLocation.getLocation().getWorld().getName();
            spawnLocations.put(x, spawnLocation);
            x++;
        }
        Log.info("Loaded " + spawnLocationTemp.size() + " Spawn Points for Map: "+tempWorld+" !");
    }
}
