package com.stimicode.SkyWarz.Listeners;

import com.stimicode.SkyWarz.Main.SkyWarz;
import com.stimicode.SkyWarz.Managers.Gui.GuiInventoryHolder;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Chest;
import org.bukkit.material.Door;
import org.bukkit.material.Redstone;
import org.bukkit.material.Sign;

/**
 * Created by Derrick on 9/12/2015.
 */
public class GuiItemListener implements Listener {
    
    @EventHandler(priority = EventPriority.NORMAL)
    public void OnBlockPlaceEvent(BlockPlaceEvent event) {
        ItemStack stack = event.getItemInHand();

        if (GuiItem.isGUIItem(stack)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void OnPlayerDropItem(PlayerDropItemEvent event) {
        ItemStack stack = event.getItemDrop().getItemStack();

        if (GuiItem.isGUIItem(stack)) {
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void OnPlayerInteract(PlayerInteractEvent event) {

        if (!event.getAction().equals(Action.RIGHT_CLICK_AIR) && !event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            return;
        }

        if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
            if (event.getClickedBlock().getState() instanceof Chest) {
                return;
            } else if (event.getClickedBlock().getState() instanceof Sign) {
                return;
            } else if (event.getClickedBlock().getType().equals(Material.WORKBENCH)) {
                return;
            } else if (event.getClickedBlock().getType().equals(Material.FURNACE)) {
                return;
            } else if (event.getClickedBlock().getState().getData() instanceof Door) {
                return;
            } else if (event.getClickedBlock().getState().getData() instanceof Redstone) {
                return;
            }
        }

        if (event.getItem() == null) {
            return;
        }

        ItemStack stack = event.getItem();
        if (!GuiItem.isGUIItem(stack)) {
            return;
        }

        String action = GuiItem.getAction(stack);
        String actionPackage = GuiItem.getActionPackage(stack, SkyWarz.getPlugin().getConfig().getString("ServerName"));
        GuiItem.processAction(actionPackage, action, stack, event);
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void OnItemSpawn(ItemSpawnEvent event) {
		/* Prevent GUI items from ever dropping and being picked up. */
        ItemStack stack = event.getEntity().getItemStack();

        if (GuiItem.isGUIItem(stack)) {
            event.setCancelled(true);
        }
    }

    /*
     * First phase of inventory click that cancels any
     * event that was clicked on a gui item.
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void OnInventoryClick(InventoryClickEvent event) {
        if (GuiItem.isGUIItem(event.getCurrentItem()) || GuiItem.isGUIItem(event.getCursor())) {
            event.setCancelled(true);

            if (event.getCurrentItem() != null) {
                String action = GuiItem.getAction(event.getCurrentItem());
                if (action == null || action.equals("")) {
                    return;
                }

                String actionPackage = GuiItem.getActionPackage(event.getCurrentItem(), SkyWarz.getPlugin().getConfig().getString("ServerName"));
                if (action != null) {
                    GuiItem.processAction(actionPackage, action, event.getCurrentItem(), event);
                }
                return;
            }

            if (event.getCursor() != null) {
                String action = GuiItem.getAction(event.getCursor());
                if (action == null || action.equals("")) {
                    return;
                }

                String actionPackage = GuiItem.getActionPackage(event.getCursor(), SkyWarz.getPlugin().getConfig().getString("ServerName"));
                if (action != null) {
                    GuiItem.processAction(actionPackage, action, event.getCursor(), event);
                }
                return;
            }
        }
    }

    /*
     * The second phase cancels the event if a non-gui item has been
     * dropped into a gui inventory.
     */
    @EventHandler(priority = EventPriority.LOW)
    public void OnInventoryClickSecondPhase(InventoryClickEvent event) {
        if (event.isCancelled()) {
            return;
        }

        if (!isGUIInventory(event.getInventory())) {
            return;
        }

        GuiInventoryHolder holder = (GuiInventoryHolder) event.getInventory().getHolder();

        if (holder.allowPlacement) {
            return;
        }

        if (event.getRawSlot() < event.getView().getTopInventory().getSize()) {
            //if (isGUIInventory(event.getInventory())) {
            event.setCancelled(true);
            //	}
        } else if (event.isShiftClick()) {
            //if (isGUIInventory(event.getInventory())) {
            event.setCancelled(true);
            //}
        }

        return;

		/*
		if (event.getRawSlot() < event.getView().getTopInventory().getSize()) {
			if (guiInventories.containsKey(event.getView().getTopInventory().getName())) {
				event.setCancelled(true);
				return;
			}
		} else if (event.isShiftClick()) {
			if (guiInventories.containsKey(event.getView().getTopInventory().getName())) {
				event.setCancelled(true);
				return;
			}
		}
		*/

    }

    @EventHandler(priority = EventPriority.HIGH)
    public void OnInventoryClickMonitor(InventoryClickEvent event) {
        if (!isGUIInventory(event.getInventory())) {
            return;
        }

        GuiInventoryHolder holder = (GuiInventoryHolder)event.getInventory().getHolder();
        if (holder.callbacks != null) {
            holder.callbacks.onClick(event);
        }
    }


    @EventHandler(priority = EventPriority.NORMAL)
    public void OnInventoryClose(InventoryCloseEvent event) {
        if (!isGUIInventory(event.getInventory())) {
            return;
        }

        GuiInventoryHolder holder = (GuiInventoryHolder)event.getInventory().getHolder();
        if (holder.callbacks != null) {
            holder.callbacks.onClose(event.getInventory());
        }
    }

    public static boolean isGUIInventory(Inventory inv) {
        return (inv.getHolder() instanceof GuiInventoryHolder);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void OnInventoryDragEvent(InventoryDragEvent event) {
        if (event.isCancelled()) {
            return;
        }

        for (int slot : event.getRawSlots()) {
            if (slot < event.getView().getTopInventory().getSize()) {
                if (isGUIInventory(event.getView().getTopInventory())) {
                    event.setCancelled(true);
                    return;
                }
            }
        }

    }
}

