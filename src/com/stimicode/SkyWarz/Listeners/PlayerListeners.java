package com.stimicode.SkyWarz.Listeners;

import com.stimicode.SkyWarz.Account.CoreAccount;
import com.stimicode.SkyWarz.Game.Game;
import com.stimicode.SkyWarz.Gui.OpenAdminCommandMenu;
import com.stimicode.SkyWarz.Managers.GameStateManager;
import com.stimicode.SkyWarz.Managers.Gui.GuiItem;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.Rank;
import com.stimicode.SkyWarz.Util.C;
import com.stimicode.SkyWarz.Util.DeathCauseUtility;
import com.stimicode.SkyWarz.Util.ItemManager;
import com.stimicode.SkyWarz.Util.PlayerUtility;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.block.Beacon;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.util.HashMap;

/**
 * Created by Derrick on 7/27/2015.
 */
public class PlayerListeners implements Listener {

    @EventHandler
    public void preLogin(PlayerLoginEvent event) {
        Player player = event.getPlayer();
        CoreAccount coreAccount = Manager.accountManager.getAccount(player);
        if (!(Game.ignoreStaff) && !(coreAccount.hasRank(Game.ignoredStaff, false))) {         //Checks if staff should be ignored, and if so what ranks. (Rank and Above)
            PlayerUtility.completelyResetPlayer(player);                                       //Resets the players state back to the default. (No items, Full health, Full food, Gamemode Survival)
        }

        if (!(Manager.gameStateManager.isState(GameStateManager.Lobby))) {
            event.disallow(PlayerLoginEvent.Result.KICK_OTHER, "Game In Progress!");
        }
    }

    /**
     *
     * @param event PlayerJoinEvent - The event that is ran when the player logs in / joins the server.
     */
    @EventHandler
    public void playerJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        CoreAccount coreAccount = Manager.accountManager.getAccount(player);

        if (!(Game.ignoreStaff) && !(coreAccount.hasRank(Game.ignoredStaff, false))) {         //Checks if staff should be ignored, and if so what ranks. (Rank and Above)
            PlayerUtility.completelyResetPlayer(player);                                       //Resets the players state back to the default. (No items, Full health, Full food, Gamemode Survival)
        }


        PlayerUtility.resetPlayerChat(player);

        if (coreAccount.hasRank(Rank.Owner, false)) {
            ItemStack temp = GuiItem.build(C.B_Red + "Admin", Material.ENDER_CHEST, 0, C.Gold + "<Click To Open>");
            temp = GuiItem.setActionClass(temp, OpenAdminCommandMenu.class);
            player.getInventory().setItem(0, temp);
            MessageManager.administrator(coreAccount, "Started work on '/ad menu' which is a GUI menu for the admin commands. Currently only the world commands have been added.","Otherstuff","morestuff");
            MessageManager.sendTitle(coreAccount, C.B_Aqua+"Welcome Administrator!", "");
        } else {
            MessageManager.sendTitle(coreAccount, C.B_Green+"Welcome To SkyWarZ!", C.B_Gold+"By OstrichMann");
        }

        player.teleport(Bukkit.getServer().getWorld("lobby").getSpawnLocation().add(0, 2, 0)); //Teleports the player to the correct Spawn Point. (Lobby Spawn Point)
    }

    @EventHandler
    public void playerQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();

        if (Manager.gameStateManager.isState(GameStateManager.InGame)) {
            Game.removeGamePlayer(player);
        }
    }

    /**
     * Handles most (if not all) code/checks related to the players movement.
     * @param event PlayerMoveEvent - The event that is ran every time the player moves their camera or person.
     */
    @EventHandler
    public void playerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        CoreAccount coreAccount = Manager.accountManager.getAccount(player);

        if (Manager.gameStateManager.isState(GameStateManager.Lobby) && (player.getLocation().getY() < 20)) {
            event.getPlayer().teleport(Bukkit.getServer().getWorld("lobby").getSpawnLocation().add(0, 2, 0));
        }

        /**
         * If the config states that we should ignore staff, we check if the player has the given rank or above and bypasses the freeze check.
         */
        if (Game.ignoreStaff && coreAccount.hasRank(Game.ignoredStaff, false)) {    //Checks if staff should be ignored, and if so what ranks. (Rank and Above)
            return;
        }

        /**
         * If the GameState is currently in the Pre-Game State we don't want players moving around, so we freeze them to their current position, which should be a spawn point.
         */
        if (Manager.gameStateManager.isState(GameStateManager.PreGame)) {           //If the GameState is currently in the Pre-Game State.
            if (event.getFrom().getBlock() != event.getTo().getBlock()) {           //Check if the player actually moved from their last position (Camera and Position)
                event.setTo(event.getFrom());                                       //Teleports the player back to their last position (Camera and Position)
            }
        }
    }

    /**
     * Handles most (if not all) code/checks related to the interactions of the players mouse buttons.
     * @param event PlayerInteractEvent - The event that is ran every time the player interacts with their mouse buttons. (Not camera movement)
     */
    @EventHandler
    public void playerInteract(PlayerInteractEvent event) {

        if (Manager.gameStateManager.isState(GameStateManager.Lobby) || Manager.gameStateManager.isState(GameStateManager.PreGame)) {
            Player player = event.getPlayer();
            CoreAccount coreAccount = Manager.accountManager.getAccount(player);
            if (Game.ignoreStaff && coreAccount.hasRank(Game.ignoredStaff, false)) {    //Checks if staff should be ignored, and if so what ranks. (Rank and Above)
                //return;
            } else {
                //event.setCancelled(true);
                //return;
            }
        }

        /**
         * If the player right clicks a Chest Block we want to fill that chest with some random loot.
         * That way we don't have to manually fill all chests at the start of the game. We only have to do it on demand.
         */
        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {                        //If the player right clicks a block.
            if (event.getClickedBlock().getType() == Material.CHEST) {              //If the block the player right clicked is a chest.
                if (!(Manager.chestManager.filledChests.contains(event.getClickedBlock().getState().getLocation()))) {
                    Manager.chestManager.fillChest((Chest) event.getClickedBlock().getState());
                    Manager.chestManager.filledChests.add((event.getClickedBlock().getState().getLocation()));
                }
            }

            if (event.getMaterial() == Material.COMMAND_MINECART || event.getMaterial() == Material.EXPLOSIVE_MINECART ||
                    event.getMaterial() == Material.HOPPER_MINECART || event.getMaterial() == Material.MINECART ||
                    event.getMaterial() == Material.POWERED_MINECART || event.getMaterial() == Material.STORAGE_MINECART) {
                Player player = event.getPlayer();
                CoreAccount coreAccount = Manager.accountManager.getAccount(player);
                if (!(coreAccount.hasRank(Rank.Administrator, false))) {
                    event.setCancelled(true);
                    MessageManager.sendError(player, "Sorry can't do that.");
                }
            }
        }
    }



    /**
     * Handles most (if not all) code/checks related to the placing of blocks.
     * @param event BlockPlaceEvent - The event that is ran every time a player places a block.
     */
    @EventHandler
     public void blockPlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        CoreAccount coreAccount = Manager.accountManager.getAccount(player);
        if (Game.ignoreStaff && coreAccount.hasRank(Game.ignoredStaff, false)) {    //Checks if staff should be ignored, and if so what ranks. (Rank and Above)
            return;                                                                 //If the player is a staff member allow them to place blocks.
        }

        if (!(Manager.gameStateManager.isState(GameStateManager.Lobby)) || !(Manager.gameStateManager.isState(GameStateManager.PreGame))) { //Check if the GameState is not Lobby or PreGame
            return;                                                                 //If not, return. Allowing them to place blocks.
        }
        event.setCancelled(true);                                                   //Otherwise cancel the event not allowing them to place blocks.
    }

    /**
     * Handles most (if not all) code/checks related to the breaking of blocks.
     * @param event BlockBreakEvent - The event that is ran every time a player breaks a block.
     */
    @EventHandler
    public void blockBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        CoreAccount coreAccount = Manager.accountManager.getAccount(player);
        if (Game.ignoreStaff && coreAccount.hasRank(Game.ignoredStaff, false)) {    //Checks if staff should be ignored, and if so what ranks. (Rank and Above)
            return;                                                                 //If the player is a staff member allow them to break blocks.
        }

        if (!(Manager.gameStateManager.isState(GameStateManager.Lobby)) || !(Manager.gameStateManager.isState(GameStateManager.PreGame))) { //Check if the GameState is not Lobby or PreGame
            return;                                                                 //If not, return. Allowing them to break blocks.
        }
        event.setCancelled(true);                                                   //Otherwise cancel the event not allowing them to break blocks.
    }

    /**
     * Handles most (if not all) code/checks related to the Death of the Player.
     * @param event PlayerDeathEvent - The event that is ran every time a player dies.
     */
    @EventHandler
    public void playerDeath(PlayerDeathEvent event) {
        new BukkitRunnable() {                                                      //This block of code simply forces the player to respawn.
            public void run() {
                try {
                    Object nmsPlayer = event.getEntity().getClass().getMethod("getHandle").invoke(event.getEntity());
                    Object con = nmsPlayer.getClass().getDeclaredField("playerConnection").get(nmsPlayer);

                    Class< ? > EntityPlayer = Class.forName(nmsPlayer.getClass().getPackage().getName() + ".EntityPlayer");

                    Field minecraftServer = con.getClass().getDeclaredField("minecraftServer");
                    minecraftServer.setAccessible(true);
                    Object mcserver = minecraftServer.get(con);

                    Object playerlist = mcserver.getClass().getDeclaredMethod("getPlayerList").invoke(mcserver);
                    Method moveToWorld = playerlist.getClass().getMethod("moveToWorld", EntityPlayer, int.class, boolean.class);
                    moveToWorld.invoke(playerlist, nmsPlayer, 0, false);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }.runTaskLater(Manager.plugin, 2);                                          //End of Code Block for Force Respawn.


        if (Game.gamePlayers.contains(event.getEntity().getPlayer().getUniqueId().toString())) {
            if (Manager.settingsManager.config.getBoolean("lightning-ondeath")) {
                event.getEntity().getWorld().strikeLightning(event.getEntity().getLocation());
            }
            Game.removeGamePlayer(event.getEntity().getPlayer());

            if (event.getEntity().getLastDamageCause() instanceof EntityDamageByEntityEvent) {
                EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) event.getEntity().getLastDamageCause();

                if (e.getDamager() instanceof Player) {
                    event.setDeathMessage(C.B_Gold + "" + event.getEntity().getName() + " has been eliminated by " + event.getEntity().getKiller().getName() + "!");
                }

                if (e.getDamager() instanceof Arrow) {
                    Arrow arrow = (Arrow) e.getDamager();

                    if (arrow.getShooter() instanceof Player) {
                        event.setDeathMessage(C.B_Gold + "" + event.getEntity().getName() + " has been eliminated by " + event.getEntity().getKiller().getName() + " with a arrow!");
                    }
                }

                if (e.getDamager() instanceof Snowball) {
                    Snowball snowball = (Snowball) e.getDamager();

                    if (snowball.getShooter() instanceof Player) {
                        event.setDeathMessage(C.B_Gold + "" + event.getEntity().getName() + " has been eliminated by " + event.getEntity().getKiller().getName() + " with a snowball!");
                    }
                }

                if (e.getDamager() instanceof Egg) {
                    Egg egg = (Egg) e.getDamager();

                    if (egg.getShooter() instanceof Player) {
                        event.setDeathMessage(C.B_Gold + "" + event.getEntity().getName() + " has been eliminated by " + event.getEntity().getKiller().getName() + "with a egg!");
                    }
                }
            } else {
                EntityDamageEvent.DamageCause cause = event.getEntity().getLastDamageCause().getCause();

                if (cause == null) {
                    return;
                }

                event.setDeathMessage(C.B_Gold + "" + event.getEntity().getName() + " has been eliminated by " + DeathCauseUtility.getCauseMessage(cause) + "!");
            }
        }
    }

    @EventHandler
    public void playerDamage(EntityDamageEvent event) {
        if (Manager.gameStateManager.isState(GameStateManager.Lobby) || Manager.gameStateManager.isState(GameStateManager.PreGame)) {
            event.setCancelled(true);
        }
    }

    //@EventHandler(priority = EventPriority.NORMAL)
    public void onChestOpen(PlayerInteractEvent event) {

        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (event.getClickedBlock().getType() == Material.CHEST || event.getClickedBlock().getType() == Material.TRAPPED_CHEST) {
                Chest chest = (Chest) event.getClickedBlock().getState();
                HashMap<String, Integer> temp = new HashMap<>();
                for (ItemStack item : chest.getInventory().getContents()) {
                    if (temp.containsKey(item.getType().name())) {
                        temp.replace(item.getType().name(), temp.get(item.getType().name()) + item.getAmount());
                    } else {
                        temp.put(item.getType().name(), item.getAmount());
                    }
                }
            }
        }
    }

    @EventHandler
    public void playerRespawn(PlayerRespawnEvent event) {
        event.getPlayer().setGameMode(GameMode.SPECTATOR);
        event.getPlayer().teleport(Bukkit.getServer().getWorld("lobby").getSpawnLocation().add(0, 2, 0));
    }

    @EventHandler
    public void mobSpawn(EntitySpawnEvent event) {
        event.setCancelled(true);
    }
}
