package com.stimicode.SkyWarz.Listeners;

import com.stimicode.SkyWarz.Account.CoreAccount;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.MessageManager;
import com.stimicode.SkyWarz.Managers.Rank;
import com.stimicode.SkyWarz.Util.C;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

/**
 * Created by Derrick on 10/14/2015.
 */
public class ChatListener implements Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    void OnPlayerAsyncChatEvent(AsyncPlayerChatEvent event) {

        CoreAccount account = Manager.accountManager.getAccount(event.getPlayer());
        if (account == null) {
            return;
        }

        event.setCancelled(true);
        Rank rank = account.getRank();
        MessageManager.sendAll(rank.get(true, true) + ChatColor.RESET + C.White + " " + event.getPlayer().getDisplayName() + ": " + event.getMessage());
    }
}
