package com.stimicode.SkyWarz.Main;

import com.stimicode.SkyWarz.Command.Admin.AdminCommand;
import com.stimicode.SkyWarz.Command.Debug.DebugCommand;
import com.stimicode.SkyWarz.Game.Game;
import com.stimicode.SkyWarz.Listeners.ChatListener;
import com.stimicode.SkyWarz.Listeners.GuiItemListener;
import com.stimicode.SkyWarz.Listeners.PlayerListeners;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.Rank;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Object.LootItem;
import com.stimicode.SkyWarz.SQL.SQL;
import com.stimicode.SkyWarz.SQL.SQLUpdate;
import com.stimicode.SkyWarz.Tasks.GameStartTimer;
import com.stimicode.SkyWarz.Tasks.GameVictoryTimer;
import com.stimicode.SkyWarz.Tasks.LobbyCloseTimer;
import com.stimicode.SkyWarz.Tasks.ServerEnd;
import com.stimicode.SkyWarz.Util.BukkitUtility;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.block.BlockState;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;

/**
 * Created by Derrick on 7/27/2015.
 */

public class SkyWarz extends JavaPlugin {

    private static JavaPlugin plugin;

    @Override
    public void onEnable() {
        setPlugin(this);
        this.saveDefaultConfig();
        init();
    }

    @Override
    public void onDisable() {
        //TODO On Disable Stuff
    }

    public void init() {
        //TODO Implement the initialization code here.
        Manager.init(this);
        initCommands();
        initListeners();
        BukkitUtility.initialize(this);
        Log.init(this);
        Manager.settingsManager.init(this);
        Rank.init();
        Game.init();

        ConfigurationSerialization.registerClass(LootItem.class, "LootItem");

        try {
            SQL.initialize();
            SQL.initObjectTables();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            initGlobals();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        TaskManager.asyncTimer("SQLUpdate", new SQLUpdate(), 1);
        TaskManager.asyncTimer("GameStartTimer", new GameStartTimer(), 20);
        TaskManager.asyncTimer("GameVictoryTimer", new GameVictoryTimer(), 20);
        TaskManager.syncTimer(new ServerEnd(), 20);
        TaskManager.syncTimer(new LobbyCloseTimer(), 20);
    }

    private void initCommands() {
        getCommand("admin").setExecutor(new AdminCommand());
        getCommand("debug").setExecutor(new DebugCommand());
    }

    private void initListeners() {
        final PluginManager pluginManager = getServer().getPluginManager();
        pluginManager.registerEvents(new GuiItemListener(), this);
        pluginManager.registerEvents(new ChatListener(), this);
        pluginManager.registerEvents(new PlayerListeners(), this);
    }

    private void initGlobals() throws SQLException {
        Manager.accountManager.loadAccounts();
    }

    public boolean hasPlugin(String name) {
        Plugin p;
        p = getServer().getPluginManager().getPlugin(name);
        return (p != null);
    }

    public static void setPlugin(JavaPlugin plugin) {
        SkyWarz.plugin = plugin;
    }

    public static JavaPlugin getPlugin() {
        return plugin;
    }
}
