package com.stimicode.SkyWarz.Main;

import com.stimicode.SkyWarz.Account.CoreAccount;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.UUID;

/**
 * Created by Derrick on 6/18/2015.
 */
public class Global {

    public static HashMap<String, CoreAccount> AccountProfiles = new HashMap<>();

    public static CoreAccount getAccount(Player player) {
        return AccountProfiles.get(player.getName().toLowerCase());
    }

    public static CoreAccount getAccount(CoreAccount account) {
        return AccountProfiles.get(account.getName().toLowerCase());
    }

    public static CoreAccount getAccount(String name) {
        return AccountProfiles.get(name.toLowerCase());
    }

    public static boolean containsAccount(String name) {
        return AccountProfiles.containsKey(name.toLowerCase());
    }

    public static void addAccount(CoreAccount account) {
        AccountProfiles.put(account.getName().toLowerCase(), account);
    }

    public static void removeAccount(CoreAccount account) {
        AccountProfiles.remove(account.getName().toLowerCase());
    }

    public static CoreAccount getAccountUUID(UUID uid) {
        for (CoreAccount account : AccountProfiles.values()) {
            if (account.getPlayer().getUniqueId().equals(uid)) {
                return account;
            }
        }
        return null;
    }

    public static Player getPlayer(CoreAccount account) throws Exception {
        Player player = Bukkit.getPlayer(account.getUuid());
        if (player == null) {
            throw new Exception("PlayerListeners "+account.getName()+" could not be located.");
        }
        return player;
    }

    @Deprecated
    public static Player getPlayer(String name) throws Exception {
        Player player = Bukkit.getPlayer(name);
        if (player == null) {
            throw new Exception("PlayerListeners "+name+" could not be located.");
        }
        return player;
    }
}
