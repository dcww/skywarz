package com.stimicode.SkyWarz.SQL;


import com.stimicode.SkyWarz.Util.NamedObject;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Derrick on 6/21/2015.
 */
public class SQLUpdateNamedObjectTask implements Runnable {

    NamedObject obj;
    HashMap<String, Object> hashmap;
    String tablename;

    public SQLUpdateNamedObjectTask(NamedObject obj, HashMap<String, Object> hashmap, String tablename) {
        this.obj = obj;
        this.hashmap = hashmap;
        this.tablename = tablename;
    }

    @Override
    public void run() {
        try {
            if (obj.getId() == 0) {
                obj.setId(SQL.insertNow(hashmap, tablename));
            } else {
                SQL.update(obj.getId(), hashmap, tablename);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}