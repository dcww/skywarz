package com.stimicode.SkyWarz.SQL;

import java.sql.SQLException;
import java.util.HashMap;

/**
 * Created by Derrick on 6/21/2015.
 */
public class SQLInsertTask  implements Runnable {

    HashMap<String, Object> hashmap;
    String tablename;

    public SQLInsertTask(HashMap<String, Object> hashmap, String tablename) {}

    @Override
    public void run() {
        try {
            SQL.insertNow(hashmap, tablename);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}