package com.stimicode.SkyWarz.SQL;

import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by Derrick on 6/21/2015.
 */
public class SQLUpdate implements Runnable {

    public static final int UPDATE_LIMIT = 50;
    private static ConcurrentLinkedQueue<com.stimicode.SkyWarz.SQL.SQLObject> saveObjects = new ConcurrentLinkedQueue<>();
    public static ConcurrentHashMap<String, Integer> saveObjectCounts = new ConcurrentHashMap<>();

    public static void add(com.stimicode.SkyWarz.SQL.SQLObject obj) {
        Integer count = saveObjectCounts.get(obj.getClass().getSimpleName());
        if (count == null) {
            count = 1;
        } else {
            count++;
        }
        saveObjectCounts.put(obj.getClass().getSimpleName(), count);
        saveObjects.add(obj);
    }

    @Override
    public void run() {
        for (int i = 0; i < UPDATE_LIMIT; i++) {
            com.stimicode.SkyWarz.SQL.SQLObject obj = saveObjects.poll();
            if (obj == null) {
                break;
            }

            try {
                Integer count = saveObjectCounts.get(obj.getClass().getSimpleName());
                if (count != null) {
                    count--;
                    saveObjectCounts.put(obj.getClass().getSimpleName(), count);
                }

                obj.saveNow();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}