package com.stimicode.SkyWarz.SQL;

import com.stimicode.SkyWarz.Account.CoreAccount;
import com.stimicode.SkyWarz.Main.Log;
import com.stimicode.SkyWarz.Managers.Manager;
import com.stimicode.SkyWarz.Managers.TaskManager;
import com.stimicode.SkyWarz.Util.NamedObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Derrick on 6/21/2015.
 */
public class SQL {
    private static String dsn = "";
    public static String hostname = "";
    public static String port = "";
    public static String db_name = "";
    public static String username = "";
    public static String password = "";
    public static String tb_prefix = "";

    public static Connection connection;

    public static void initialize() throws Exception {
        Log.heading("Initializing SQL");

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            e.printStackTrace();
        }

        SQL.hostname = Manager.settingsManager.getStringBase("mysql.hostname");
        SQL.port = Manager.settingsManager.getStringBase("mysql.port");
        SQL.db_name = Manager.settingsManager.getStringBase("mysql.database");
        SQL.username = Manager.settingsManager.getStringBase("mysql.username");
        SQL.password = Manager.settingsManager.getStringBase("mysql.password");
        SQL.tb_prefix = Manager.settingsManager.getStringBase("mysql.table_prefix");
        SQL.dsn = "jdbc:mysql://" + hostname + ":" + port + "/" + db_name;

        Log.info("\t Using " + SQL.tb_prefix + SQL.db_name + " as database.");
        Log.heading("Initializing SQL Finished");
    }

    public synchronized static void openConnection() {
        try {
            connection = DriverManager.getConnection(SQL.dsn, SQL.username, SQL.password);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Connection getConnection() {
        SQL.openConnection();
        return SQL.connection;
    }

    public static void initObjectTables() throws SQLException {
        Log.heading("Building SQL Object Tables.");
        CoreAccount.init();
        Log.info("----- Done Building Tables ----");
    }

    public static boolean hasTable(String name) throws SQLException {
        Connection context = null;
        ResultSet result = null;
        try {
            context = getConnection();
            DatabaseMetaData dbm = context.getMetaData();
            String[] types = { "TABLE" };

            result = dbm.getTables(null, null, SQL.tb_prefix + name, types);
            return result.next();
        } finally {
            SQL.close(result, null, context);
        }
    }

    public static boolean hasColumn(String tablename, String columnName) throws SQLException {
        Connection context = null;
        ResultSet result = null;

        try {
            context = getConnection();
            DatabaseMetaData dbm = context.getMetaData();
            result = dbm.getColumns(null, null, SQL.tb_prefix + tablename, columnName);
            return result.next();
        } finally {
            SQL.close(result, null, context);
        }
    }

    public static void addColumn(String tablename, String columnDef) throws SQLException {
        Connection context = null;
        PreparedStatement ps = null;

        try {
            String table_alter = "ALTER TABLE "+ SQL.tb_prefix + tablename +" ADD " +columnDef;
            context = getConnection();
            ps = context.prepareStatement(table_alter);
            ps.execute();
            Log.info("\tADDED:"+columnDef);
        } finally {
            SQL.close(null, ps, context);
        }

    }

    public static void updateNamedObjectAsync(NamedObject obj, HashMap<String, Object> hashmap, String tablename) {
        TaskManager.asyncTask("", new SQLUpdateNamedObjectTask(obj, hashmap, tablename), 0);
    }

    public static void updateNamedObject(SQLObject obj, HashMap<String, Object> hashmap, String tablename) throws SQLException {
        if (obj.isDeleted()) {
            return;
        }

        Log.info("OBJECT Object ID "+obj.getId());
        if (obj.getId() == 0) {
            obj.setId(SQL.insertNow(hashmap, tablename));
        } else {
            SQL.update(obj.getId(), hashmap, tablename);
        }
    }

    public static void update(int id, HashMap<String, Object> hashmap, String tablename) throws SQLException {
        hashmap.put("id", id);
        update(hashmap, "id", tablename);
    }


    public static void update(HashMap<String,Object> hashmap, String keyname, String tablename) throws SQLException {
        Connection context = null;
        PreparedStatement ps = null;

        try {
            String sql = "UPDATE " + SQL.tb_prefix + tablename + " SET ";
            String where = " WHERE `"+keyname+"` = ?;";
            ArrayList<Object> values = new ArrayList<>();

            Object keyValue = hashmap.get(keyname);
            hashmap.remove(keyname);

            Iterator<String> keyIter = hashmap.keySet().iterator();
            while (keyIter.hasNext()) {
                String key = keyIter.next();
                sql += "`"+key+"` = ?";
                sql += "" + (keyIter.hasNext() ? ", " : " ");
                values.add(hashmap.get(key));
            }

            sql += where;
            context = SQL.getConnection();
            ps = context.prepareStatement(sql);

            int i = 1;
            for (Object value : values) {
                if (value instanceof String) {
                    ps.setString(i, (String) value);
                } else if (value instanceof Integer) {
                    ps.setInt(i, (Integer)value);
                } else if (value instanceof Boolean) {
                    ps.setBoolean(i, (Boolean)value);
                } else if (value instanceof Double) {
                    ps.setDouble(i, (Double)value);
                } else if (value instanceof Float) {
                    ps.setFloat(i, (Float)value);
                } else if (value instanceof Long) {
                    ps.setLong(i, (Long)value);
                } else {
                    ps.setObject(i, value);
                }
                i++;
            }

            ps.setObject(i, keyValue);

            if (ps.executeUpdate() == 0) {
                insertNow(hashmap, tablename);
            }
        } finally {
            SQL.close(null, ps, context);
        }
    }

    public static void insert(HashMap<String, Object> hashmap, String tablename) {
        TaskManager.asyncTask(new SQLInsertTask(hashmap, tablename), 0);
    }

    public static int insertNow(HashMap<String, Object> hashmap, String tablename) throws SQLException {
        Connection context = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        try {
            String sql = "INSERT INTO `" + SQL.tb_prefix + tablename + "` ";
            String keycodes = "(";
            String valuecodes = " VALUES (";
            ArrayList<Object> values = new ArrayList<>();

            Iterator<String> keyIter = hashmap.keySet().iterator();
            while (keyIter.hasNext()) {
                String key = keyIter.next();

                keycodes += key;
                keycodes += "" + (keyIter.hasNext() ? "," : ")");

                valuecodes += "?";
                valuecodes += "" + (keyIter.hasNext() ? "," : ")");

                values.add(hashmap.get(key));
            }

            sql += keycodes;
            sql += valuecodes;

            context = SQL.getConnection();
            ps = context.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            Log.info("Prepared Statement: "+ps.toString());

            int i = 1;
            for (Object value : values) {
                Log.info("Values: "+value.toString());
                if (value instanceof String) {
                    ps.setString(i, (String) value);
                } else if (value instanceof Integer) {
                    ps.setInt(i, (Integer)value);
                } else if (value instanceof Boolean) {
                    ps.setBoolean(i, (Boolean)value);
                } else if (value instanceof Double) {
                    ps.setDouble(i, (Double)value);
                } else if (value instanceof Float) {
                    ps.setFloat(i, (Float)value);
                } else if (value instanceof Long) {
                    ps.setLong(i, (Long)value);
                } else {
                    ps.setObject(i, value);
                }
                i++;
            }

            try {
                Log.info("Statement : " + ps.execute());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            int id = 0;
            rs = ps.getGeneratedKeys();

            while (rs.next()) {
                id = rs.getInt(1);
                Log.info("RESULTSET: "+id);
                break;
            }

            if (id == 0) {
                String name = (String)hashmap.get("name");
                if (name == null) {
                    name = "Unknown";
                }

                Log.error("SQL ERROR: Saving an SQLObject returned a 0 ID! Name:"+name+" Table:"+tablename);
            }
            return id;

        } finally {
            SQL.close(rs, ps, context);
        }
    }


    public static void deleteObjectById(SQLObject obj, String tablename) throws SQLException {
        Connection context = null;
        PreparedStatement ps = null;

        try {
            String sql = "DELETE FROM " + SQL.tb_prefix + tablename + " WHERE `id` = ?";
            context = SQL.getConnection();
            ps = context.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, obj.getId());
            ps.execute();
            ps.close();
            obj.setDeleted(true);
        } finally {
            SQL.close(null, ps, context);
        }
    }

    public static void deleteByName(String name, String tablename) throws SQLException {
        Connection context = null;
        PreparedStatement ps = null;

        try {
            String sql = "DELETE FROM " + SQL.tb_prefix + tablename + " WHERE `name` = ?";
            context = SQL.getConnection();
            ps = context.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, name);
            ps.execute();
            ps.close();
        } finally {
            SQL.close(null, ps, context);
        }
    }

    public static void makeCol(String colname, String type, String TABLE_NAME) throws SQLException {
        if (!SQL.hasColumn(TABLE_NAME, colname)) {
            Log.info("\tCouldn't find " + colname + " column for " + TABLE_NAME);
            SQL.addColumn(TABLE_NAME, "`" + colname + "` " + type);
        }
    }

    public static void makeTable(String table_create) throws SQLException {
        Connection context = null;
        PreparedStatement ps = null;

        try {
            context = SQL.getConnection();
            ps = context.prepareStatement(table_create);
            ps.execute();
        } finally {
            SQL.close(null, ps, context);
        }

    }

    public static void close(ResultSet rs, PreparedStatement ps, Connection context) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (ps != null) {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (context != null) {
            try {
                context.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
