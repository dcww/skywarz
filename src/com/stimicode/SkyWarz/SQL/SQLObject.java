package com.stimicode.SkyWarz.SQL;


import com.stimicode.SkyWarz.Util.NamedObject;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Derrick on 6/21/2015.
 */
public abstract class SQLObject extends NamedObject {
    private boolean isDeleted = false;

    public abstract void load(ResultSet rs) throws Exception;

    public abstract void save();

    public abstract void saveNow() throws SQLException;

    @SuppressWarnings("EmptyMethod")
    public abstract void delete() throws SQLException;

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }
}
