# README #

SkyWarz Plugin

### Features ###

* Customizable Player Count
* Multiple Map Support
* Map Management System (Create, Load, Save, Reset Maps, Map Templates)
* Chest Loot Management
* Complete Customization via Configs
* Gui System